webpackJsonp([0],[
/* 0 */,
/* 1 */,
/* 2 */,
/* 3 */,
/* 4 */,
/* 5 */,
/* 6 */,
/* 7 */,
/* 8 */,
/* 9 */,
/* 10 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_vue_router__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_Hello__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_Hello___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2__components_Hello__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_login__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_login___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3__components_login__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__components_signup__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__components_signup___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4__components_signup__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__components_addnewuser__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__components_addnewuser___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5__components_addnewuser__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__components_adduser__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__components_adduser___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6__components_adduser__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__components_dashboard__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__components_dashboard___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7__components_dashboard__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__components_allprojects__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__components_allprojects___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8__components_allprojects__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__components_myprojects__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__components_myprojects___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9__components_myprojects__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__components_addproject__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__components_addproject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_10__components_addproject__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__components_services__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__components_services___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_11__components_services__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__components_addservice__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__components_addservice___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_12__components_addservice__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__components_api__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__components_api___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_13__components_api__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__components_addAPI__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__components_addAPI___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_14__components_addAPI__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__components_documentation__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__components_documentation___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_15__components_documentation__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__components_index__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__components_index___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_16__components_index__);


















__WEBPACK_IMPORTED_MODULE_0_vue__["a" /* default */].use(__WEBPACK_IMPORTED_MODULE_1_vue_router__["a" /* default */]);
/* harmony default export */ __webpack_exports__["a"] = (new __WEBPACK_IMPORTED_MODULE_1_vue_router__["a" /* default */]({
    routes: [{
        path: '/',
        name: 'login',
        component: __WEBPACK_IMPORTED_MODULE_3__components_login___default.a
    }, {
        path: '/signup',
        name: 'signup',
        component: __WEBPACK_IMPORTED_MODULE_4__components_signup___default.a
    }, {
        path: '/dashboard',
        name: 'dashboard',
        component: __WEBPACK_IMPORTED_MODULE_7__components_dashboard___default.a,
        children: [{ path: 'index', name: 'index', component: __WEBPACK_IMPORTED_MODULE_16__components_index___default.a }, { path: 'allprojects', name: 'allprojects', component: __WEBPACK_IMPORTED_MODULE_8__components_allprojects___default.a }, { path: 'myprojects', name: 'myprojects', component: __WEBPACK_IMPORTED_MODULE_9__components_myprojects___default.a }, { path: 'addproject', name: 'addproject', component: __WEBPACK_IMPORTED_MODULE_10__components_addproject___default.a }, { path: 'addnewuser', name: 'addnewuser', component: __WEBPACK_IMPORTED_MODULE_5__components_addnewuser___default.a }, { path: 'adduser', name: 'adduser', component: __WEBPACK_IMPORTED_MODULE_6__components_adduser___default.a },
        //  { path: 'services', name: 'services', component: services },
        { path: 'services/:id', name: 'services', component: __WEBPACK_IMPORTED_MODULE_11__components_services___default.a }, { path: 'addservice/:id', name: 'services', component: __WEBPACK_IMPORTED_MODULE_12__components_addservice___default.a }, { path: 'api/:id', name: 'api', component: __WEBPACK_IMPORTED_MODULE_13__components_api___default.a }, { path: 'documentation/:id', name: 'documentation', component: __WEBPACK_IMPORTED_MODULE_15__components_documentation___default.a }, { path: 'addAPI/:id', name: 'addAPI', component: __WEBPACK_IMPORTED_MODULE_14__components_addAPI___default.a }]
    }]
}));

/***/ }),
/* 11 */
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(28)

var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(12),
  /* template */
  __webpack_require__(59),
  /* scopeId */
  "data-v-137e5e61",
  /* cssModules */
  null
)

module.exports = Component.exports


/***/ }),
/* 12 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//

window.foo = "fooo";
/* harmony default export */ __webpack_exports__["default"] = ({
    name: 'app',
    data() {
        return {};
    }
});

/***/ }),
/* 13 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    name: 'hello',
    data() {
        return {
            msg: 'Welcome to Your Vue.js App'
        };
    }
});

/***/ }),
/* 14 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_sweetalert__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_sweetalert___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_sweetalert__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_vue__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_vue_router__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_vue_resource__ = __webpack_require__(3);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//





__WEBPACK_IMPORTED_MODULE_1_vue__["a" /* default */].use(__WEBPACK_IMPORTED_MODULE_3_vue_resource__["a" /* default */]);
__WEBPACK_IMPORTED_MODULE_1_vue__["a" /* default */].use(__WEBPACK_IMPORTED_MODULE_2_vue_router__["a" /* default */]);
/* harmony default export */ __webpack_exports__["default"] = ({
    name: 'adddocumentation',
    data() {
        return {
            apiType: '',
            sid: '',
            url: '',
            key: '',
            value: '',
            field: '',
            type: '',
            description: '',
            headerParameter: [],
            params: [],
            exampleUsage: '',
            successResponse: '',
            errorResponse: ''
        };
    },
    methods: {
        addheader() {
            this.headerParameter.push({
                "key": this.key,
                "value": this.value
            });
            this.key = '';
            this.value = '';
        },
        addparams() {
            this.params.push({
                "field": this.field,
                "type": this.type,
                "description": this.description
            });
            this.field = '';
            this.type = '';
            this.description = '';
        },
        add() {
            this.userid = localStorage.getItem('userid');
            var obj = {
                'documentation': {
                    "url": this.url,
                    "apiType": this.apiType,
                    "headerParameter": this.headerParameter,
                    "params": this.params,
                    "exampleUsage": this.exampleUsage,
                    "successResponse": this.successResponse,
                    "errorResponse": this.errorResponse
                }
            };
            this.sid = this.$route.params.id;;
            console.log(this.userid);
            console.log("IpAddress" + globalLocalHost);
            this.$http.post(globalLocalHost + '/addDocumentation/' + this.sid, obj).then(response => {
                if (response.body.err == false) {
                    __WEBPACK_IMPORTED_MODULE_0_sweetalert___default()('Success!', 'documentation Added successfully', 'success');
                    console.log(response);
                    this.$router.push('/dashboard/api/' + this.sid);
                } else {
                    console.log(response);
                    __WEBPACK_IMPORTED_MODULE_0_sweetalert___default()('Oops...', 'Something went wrong!', 'error');
                }
            }, response => {
                console.log(response);
                __WEBPACK_IMPORTED_MODULE_0_sweetalert___default()('Oops...', 'Something went wrong!', 'error');
            });
        },
        cancel() {
            var id = this.$route.params.id;
            this.$router.push('/dashboard/api/' + id);
        }
    }
});

/***/ }),
/* 15 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_sweetalert__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_sweetalert___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_sweetalert__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_vue__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_vue_resource__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_vue_router__ = __webpack_require__(4);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//





__WEBPACK_IMPORTED_MODULE_1_vue__["a" /* default */].use(__WEBPACK_IMPORTED_MODULE_2_vue_resource__["a" /* default */]);
__WEBPACK_IMPORTED_MODULE_1_vue__["a" /* default */].use(__WEBPACK_IMPORTED_MODULE_3_vue_router__["a" /* default */]);
/* harmony default export */ __webpack_exports__["default"] = ({
    name: 'signup',
    data() {
        return {
            email: '',
            password: '',
            firstName: '',
            lastName: '',
            password: '',
            userrole: '',
            username: '',
            status: '',
            usertype: '',
            mobile: ''
        };
    },
    methods: {
        signup() {
            var obj = {
                'user': {
                    'firstName': this.firstName,
                    'lastName': this.lastName,
                    'username': this.username,
                    'email': this.email,
                    'password': this.password,
                    'userrole': this.userrole,
                    'status': this.status,
                    'usertype': 'user',
                    'mobile': this.mobile
                }
                //added globalLocalHost value to change it change from global.js
            };console.log("+++" + globalLocalHost);
            this.$http.post(globalLocalHost + '/addUser', obj).then(response => {
                if (response.body.err === false) {
                    __WEBPACK_IMPORTED_MODULE_0_sweetalert___default()('Success!', 'usser added successsfully', 'success');
                    this.$router.push({
                        path: '/dashboard/adduser'
                    });
                } else {
                    console.log(response);
                    __WEBPACK_IMPORTED_MODULE_0_sweetalert___default()('Oops...', 'Something went wrong! email and mobile already exist', 'error');
                }
            }, response => {});
        }

    }
});

/***/ }),
/* 16 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_sweetalert__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_sweetalert___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_sweetalert__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_vue__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_vue_router__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_vue_resource__ = __webpack_require__(3);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//





__WEBPACK_IMPORTED_MODULE_1_vue__["a" /* default */].use(__WEBPACK_IMPORTED_MODULE_3_vue_resource__["a" /* default */]);
__WEBPACK_IMPORTED_MODULE_1_vue__["a" /* default */].use(__WEBPACK_IMPORTED_MODULE_2_vue_router__["a" /* default */]);
/* harmony default export */ __webpack_exports__["default"] = ({
    name: 'addproject',
    data() {
        return {
            name: '',
            isPrivate: '',
            purpose: '',
            status: '',
            authenticationDescription: '',
            userid: ''
        };
    },
    methods: {
        add() {
            this.userid = localStorage.getItem('userid');
            var obj = {
                'project': {
                    'name': this.name,
                    'isPrivate': this.isPrivate,
                    'purpose': this.purpose,
                    'status': this.status,
                    'authenticationDescription': this.authenticationDescription
                }
            };
            this.userid = localStorage.getItem('userId');
            console.log(this.userid);
            console.log("ipAddress:" + globalLocalHost);
            this.$http.post(globalLocalHost + '/user/addProject/' + this.userid, obj).then(response => {
                if (response.body.err == false) {
                    __WEBPACK_IMPORTED_MODULE_0_sweetalert___default()('Success!', 'Project Added successfully', 'success');
                    console.log(response);
                    this.$router.push('/dashboard/allprojects');
                } else {
                    console.log(response);
                    __WEBPACK_IMPORTED_MODULE_0_sweetalert___default()('Oops...', 'Something went wrong!', 'error');
                }
            }, response => {
                console.log(response);
                __WEBPACK_IMPORTED_MODULE_0_sweetalert___default()('Oops...', 'Something went wrong!', 'error');
            });
        },
        cancelop() {
            this.$router.push('/dashboard/allprojects');
        }
    }
});

/***/ }),
/* 17 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_sweetalert__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_sweetalert___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_sweetalert__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_vue__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_vue_router__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_vue_resource__ = __webpack_require__(3);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//





__WEBPACK_IMPORTED_MODULE_1_vue__["a" /* default */].use(__WEBPACK_IMPORTED_MODULE_3_vue_resource__["a" /* default */]);
__WEBPACK_IMPORTED_MODULE_1_vue__["a" /* default */].use(__WEBPACK_IMPORTED_MODULE_2_vue_router__["a" /* default */]);
/* harmony default export */ __webpack_exports__["default"] = ({
    name: 'addservice',
    data() {
        return {
            name: '',
            status: '',
            description: '',
            userid: ''
        };
    },
    methods: {
        add() {
            this.userid = localStorage.getItem('userid');
            var obj = {
                'service': {
                    'name': this.name,
                    'status': this.status,
                    'description': this.description
                }
            };
            this.userid = localStorage.getItem('userId');
            var id = this.$route.params.id;
            console.log(this.userid);
            console.log("IpAddress" + globalLocalHost);
            this.$http.post(globalLocalHost + '/user/addProjectServie/' + this.userid + '/' + id, obj).then(response => {
                if (response.body.err == false) {
                    __WEBPACK_IMPORTED_MODULE_0_sweetalert___default()('Success!', 'service Added successfully', 'success');
                    console.log(response);
                    this.$router.push('/dashboard/services/' + id);
                } else {
                    console.log(response);
                    __WEBPACK_IMPORTED_MODULE_0_sweetalert___default()('Oops...', 'Something went wrong!', 'error');
                }
            }, response => {
                console.log(response);
                __WEBPACK_IMPORTED_MODULE_0_sweetalert___default()('Oops...', 'Something went wrong!', 'error');
            });
        },
        cancel() {
            var id = this.$route.params.id;
            this.$router.push('/dashboard/services/' + id);
        }
    }
});

/***/ }),
/* 18 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_sweetalert__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_sweetalert___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_sweetalert__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_vue__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_vue_router__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_vue_resource__ = __webpack_require__(3);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//





__WEBPACK_IMPORTED_MODULE_1_vue__["a" /* default */].use(__WEBPACK_IMPORTED_MODULE_3_vue_resource__["a" /* default */]);
__WEBPACK_IMPORTED_MODULE_1_vue__["a" /* default */].use(__WEBPACK_IMPORTED_MODULE_2_vue_router__["a" /* default */]);
/* harmony default export */ __webpack_exports__["default"] = ({

    name: 'login',
    data() {
        return {
            //project details
            firstName: '',
            name: '',
            isPrivate: '',
            status: '',
            email: '',
            password: '',
            users: [],
            prjId: '',
            url: '',
            username: '',
            lastName: '',
            mobile: '',
            usertype: '',
            userrole: ''

        };
    },

    methods: {

        getAllusers() {
            console.log("ipaddress:" + globalLocalHost);
            this.$http.get(globalLocalHost + '/getAllUser').then(response => {
                if (response.body.err == false) {
                    //sweetalert('Success!', 'Login successful', 'success');
                    console.log(response);
                    this.users = response.body.result.users;
                } else {

                    __WEBPACK_IMPORTED_MODULE_0_sweetalert___default()('Oops...', 'Something went wrong!', 'error');
                }
            }, response => {
                console.log(response);
                __WEBPACK_IMPORTED_MODULE_0_sweetalert___default()('Oops...', 'Something went wrong!', 'error');
            });
        },
        deleteuser(user) {
            debugger;
            this.userId = user._id;
            this.$http.delete(globalLocalHost + '/deleteUser/' + this.userId).then(response => {
                if (response.body.err == false) {
                    console.log(response);
                    this.users.splice(this.users.indexOf(user), 1);
                    __WEBPACK_IMPORTED_MODULE_0_sweetalert___default()('Success!', 'project deleted  successfully', 'success');
                    console.log("deleted");
                } else {
                    console.log(response);
                    __WEBPACK_IMPORTED_MODULE_0_sweetalert___default()('Oops...', 'Something went wrong!', 'error');
                }
            }, response => {
                __WEBPACK_IMPORTED_MODULE_0_sweetalert___default()('Oops...', 'Something went wrong!', 'error');
                console.log(response);
            });
        },
        div_show1(user) {
            this.userId = user._id;
            console.log("here");
            document.getElementById('frm1').style.display = "block";
            document.getElementById('tb').style.display = "none";

            this.firstName = user.firstName, this.lastName = user.lastName, this.status = user.status, this.mobile = user.mobile, this.username = user.username, this.email = user.email, this.usertype = user.usertype, this.userrole = user.userrole;
        },
        div_hide1() {
            document.getElementById('frm1').style.display = "none";
            document.getElementById('tb').style.display = "block";
            console.log("call edit button");
            //create json for update api
            var obj = {
                'user': {
                    'firstName': this.firstName,
                    'lastName': this.lastName,
                    'status': this.status,
                    'mobile': this.mobile,
                    'username': this.username,
                    'email': this.email,
                    'usertype': this.usertype,
                    'userrole': this.userrole
                }
                //call edit api
            };this.userid = localStorage.getItem('userId');

            this.$http.put(globalLocalHost + '/updateuser/' + this.userId, obj).then(response => {
                if (response.body.err == false) {
                    console.log(response);
                    this.getAllusers();
                    __WEBPACK_IMPORTED_MODULE_0_sweetalert___default()('Success!', 'update done successfully', 'success');
                } else {
                    console.log(response);
                    __WEBPACK_IMPORTED_MODULE_0_sweetalert___default()('Oops...', 'Something went wrong!', 'error');
                }
            }, response => {
                console.log(response);
                __WEBPACK_IMPORTED_MODULE_0_sweetalert___default()('Oops...', 'Something went wrong!', 'error');
            });
        },
        cancelop() {
            document.getElementById('frm1').style.display = "none";
            document.getElementById('tb').style.display = "block";
        }

    },
    created() {
        this.getAllusers();
    }

});

/***/ }),
/* 19 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_sweetalert__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_sweetalert___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_sweetalert__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_vue__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_vue_router__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_vue_resource__ = __webpack_require__(3);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//






__WEBPACK_IMPORTED_MODULE_1_vue__["a" /* default */].use(__WEBPACK_IMPORTED_MODULE_3_vue_resource__["a" /* default */]);
__WEBPACK_IMPORTED_MODULE_1_vue__["a" /* default */].use(__WEBPACK_IMPORTED_MODULE_2_vue_router__["a" /* default */]);
/* harmony default export */ __webpack_exports__["default"] = ({

    name: 'login',
    data() {
        return {
            //project details
            name: '',
            isPrivate: '',
            purpose: '',
            status: '',
            authenticationDescription: '',
            email: '',
            password: '',
            projects: [],
            prjId: '',
            url: ''
        };
    },

    methods: {
        services(project) {
            this.$router.push('/dashboard/services/' + project._id);
        },
        getAllProjects() {
            console.log("ipaddress:" + globalLocalHost);
            this.$http.get(globalLocalHost + '/user/getAllProject').then(response => {
                if (response.body.err == false) {
                    //sweetalert('Success!', 'Login successful', 'success');
                    console.log(response);
                    this.projects = response.body.result.project;
                } else {

                    __WEBPACK_IMPORTED_MODULE_0_sweetalert___default()('Oops...', 'Something went wrong!', 'error');
                }
            }, response => {
                console.log(response);
                __WEBPACK_IMPORTED_MODULE_0_sweetalert___default()('Oops...', 'Something went wrong!', 'error');
            });
        },
        addproject() {
            this.$router.push('/dashboard/addproject');
        },
        deleteproject(project) {
            debugger;
            this.prjId = project._id;
            this.userid = localStorage.getItem('userId');
            //  console.log("projectis:" + pid)
            console.log("ownerId:" + this.userid);
            console.log("ipaddress:" + globalLocalHost);
            this.$http.delete(globalLocalHost + '/owner/deleteProject/' + this.prjId + '/' + this.userid).then(response => {
                if (response.body.err == false) {
                    console.log(response);
                    this.projects.splice(this.projects.indexOf(project), 1);
                    __WEBPACK_IMPORTED_MODULE_0_sweetalert___default()('Success!', 'project deleted  successfully', 'success');
                    console.log("deleted");
                } else {
                    console.log(response);
                    __WEBPACK_IMPORTED_MODULE_0_sweetalert___default()('Oops...', 'Something went wrong!', 'error');
                }
            }, response => {
                __WEBPACK_IMPORTED_MODULE_0_sweetalert___default()('Oops...', 'Something went wrong!', 'error');
                console.log(response);
            });
        },
        div_show1(project) {
            this.prjId = project._id;
            console.log("here");
            document.getElementById('frm1').style.display = "block";
            document.getElementById('tb').style.display = "none";
            //add values to edit form
            console.log("here+++++");
            console.log(project.name);
            this.name = project.name, this.isPrivate = project.isPrivate, this.purpose = project.purpose, this.status = project.status, this.authenticationDescription = project.authenticationDescription;
        },
        div_hide1() {
            document.getElementById('frm1').style.display = "none";
            document.getElementById('tb').style.display = "block";
            console.log("call edit button");
            //create json for update api
            var obj = {
                'project': {
                    'name': this.name,
                    'isPrivate': this.isPrivate,
                    'purpose': this.purpose,
                    'status': this.status,
                    'authenticationDescription': this.authenticationDescription
                }
                //call edit api
            };this.userid = localStorage.getItem('userId');
            console.log("projects:" + this.prjId);
            console.log("ownerId:" + this.userid);
            console.log("IpAddress:" + globalLocalHost);
            this.$http.put(globalLocalHost + '/owner/updateProject/' + this.userid + '/' + this.prjId, obj).then(response => {
                if (response.body.err == false) {
                    console.log(response);
                    this.getAllProjects();
                    __WEBPACK_IMPORTED_MODULE_0_sweetalert___default()('Success!', 'update done successfully', 'success');
                } else {
                    console.log(response);
                    __WEBPACK_IMPORTED_MODULE_0_sweetalert___default()('Oops...', 'Something went wrong!', 'error');
                }
            }, response => {
                console.log(response);
                __WEBPACK_IMPORTED_MODULE_0_sweetalert___default()('Oops...', 'Something went wrong!', 'error');
            });
        },
        cancelop() {
            document.getElementById('frm1').style.display = "none";
            document.getElementById('tb').style.display = "block";
        },
        exportprj(project) {
            debugger;
            //this.prjId = project._id;
            console.log(project._id);
            this.url = globalLocalHost + '/getallservicesofproject/' + project._id;
            // console.log(this.url);
            window.open(this.url, "_blank");
            //  this.$http.get(globalLocalHost + '/getallservicesofproject/' + this.prjId ).then(response => {
            //     if (response.body.err == false) {
            //         console.log(response)
            //         // this.projects.splice(this.projects.indexOf(project), 1)
            //         // sweetalert('Success!', 'project deleted  successfully', 'success');
            //         console.log("exported successfully")
            //     } else {
            //         console.log(response)
            //         sweetalert('Oops...', 'Something went wrong!', 'error')
            //     }
            // }, response => {
            //     sweetalert('Oops...', 'Something went wrong!', 'error')
            //     console.log(response)
            // })
        }
    },
    created() {
        this.getAllProjects();
    }

});

/***/ }),
/* 20 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_sweetalert__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_sweetalert___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_sweetalert__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_vue__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_vue_router__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_vue_resource__ = __webpack_require__(3);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//





__WEBPACK_IMPORTED_MODULE_1_vue__["a" /* default */].use(__WEBPACK_IMPORTED_MODULE_3_vue_resource__["a" /* default */]);
__WEBPACK_IMPORTED_MODULE_1_vue__["a" /* default */].use(__WEBPACK_IMPORTED_MODULE_2_vue_router__["a" /* default */]);
/* harmony default export */ __webpack_exports__["default"] = ({
    name: 'login',
    data() {
        return {
            //service details
            name: '',
            status: '',
            description: '',
            APIs: [],
            headerParameter: [],
            params: [],
            exampleUsage: {},
            successResponse: {},
            errorResponse: {},
            apiType: '',
            url: '',
            key: '',
            value: '',
            field: '',
            type: '',
            seen: true,
            show: true,
            did: ''
        };
    },
    methods: {
        cancelop() {
            document.getElementById('frm1').style.display = "none";
            document.getElementById('tb').style.display = "block";
        },
        addheader() {
            this.headerParameter.push({
                "key": this.key,
                "value": this.value
            });
            this.key = '';
            this.value = '';
        },
        addparams() {
            this.params.push({
                "field": this.field,
                "type": this.type,
                "description": this.description
            });
            this.field = '';
            this.type = '';
            this.description = '';
        },
        //use
        getAllservices() {
            var id = this.$route.params.id;
            console.log("IpAddress" + globalLocalHost);
            this.$http.get(globalLocalHost + '/getAlldocumentOfService/' + id).then(response => {
                if (response.body.err == false) {
                    //sweetalert('Success!', 'Login successful', 'success');
                    console.log(response);
                    this.APIs = response.body.result;
                } else {
                    __WEBPACK_IMPORTED_MODULE_0_sweetalert___default()('Oops...', 'Something went wrong!', 'error');
                }
            }, response => {
                console.log(response);
                __WEBPACK_IMPORTED_MODULE_0_sweetalert___default()('Oops...', 'Something went wrong!', 'error');
            });
        },
        //use
        addAPI() {
            var id = this.$route.params.id;
            this.$router.push('/dashboard/addAPI/' + id);
        },
        //use
        deleteapi(api) {
            this.did = api._id;
            this.userid = localStorage.getItem('userId');
            console.log("IpAddress" + globalLocalHost);
            // debugger
            this.$http.delete(globalLocalHost + '/deleteDocument/' + this.did).then(response => {
                if (response.body.err == false) {
                    console.log(response);
                    this.APIs.splice(this.APIs.indexOf(api), 1);
                    __WEBPACK_IMPORTED_MODULE_0_sweetalert___default()('Success!', 'documentation deleted  successfully', 'success');
                    console.log("deleted");
                } else {
                    console.log(response);
                    __WEBPACK_IMPORTED_MODULE_0_sweetalert___default()('Oops...', 'Something went wrong!', 'error');
                }
            }, response => {
                __WEBPACK_IMPORTED_MODULE_0_sweetalert___default()('Oops...', 'Something went wrong!', 'error');
                console.log(response);
            });
        },
        div_show1(service) {
            this.did = service._id;
            console.log("here");
            document.getElementById('frm1').style.display = "block";
            document.getElementById('tb').style.display = "none";
            //add values to edit form
            this.apiType = service.apiType, this.url = service.url, this.headerParameter = service.headerParameter, this.params = service.params, this.exampleUsage = service.exampleUsage, this.successResponse = service.successResponse, this.errorResponse = service.errorResponse;
        },
        div_hide1() {
            document.getElementById('frm1').style.display = "none";
            document.getElementById('tb').style.display = "block";
            console.log("call edit button");
            //create json for update api
            var obj = {
                'documentation': {
                    "url": this.url,
                    "apiType": this.apiType,
                    "headerParameter": this.headerParameter,
                    "params": this.params,
                    "exampleUsage": this.exampleUsage,
                    "successResponse": this.successResponse,
                    "errorResponse": this.errorResponse
                }
                //call edit api
            };var pid = this.$route.params.id;
            this.userid = localStorage.getItem('userId');
            console.log("services:" + pid);
            console.log("ownerId:" + this.userid);
            console.log("IpAddress" + globalLocalHost);
            //   debugger
            this.$http.put(globalLocalHost + '/UpDdateocumentation/' + this.did, obj).then(response => {
                if (response.body.err == false) {
                    console.log(response);
                    this.getAllservices();
                    __WEBPACK_IMPORTED_MODULE_0_sweetalert___default()('Success!', 'updated seccessfully done', 'success');
                } else {
                    console.log(response);
                    __WEBPACK_IMPORTED_MODULE_0_sweetalert___default()('Oops...', 'Something went wrong!', 'error');
                }
            }, response => {
                __WEBPACK_IMPORTED_MODULE_0_sweetalert___default()('Oops...', 'Something went wrong!', 'error');
            });
        },
        documentation(api) {
            var ser_id = this.$route.params.id;
            localStorage.setItem('ser_id', ser_id);
            this.$router.push('/dashboard/documentation/' + api._id);
        }
    },

    created() {
        this.getAllservices();
    }
});

/***/ }),
/* 21 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_sweetalert__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_sweetalert___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_sweetalert__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_vue__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_vue_resource__ = __webpack_require__(3);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




__WEBPACK_IMPORTED_MODULE_1_vue__["a" /* default */].use(__WEBPACK_IMPORTED_MODULE_2_vue_resource__["a" /* default */]);

__WEBPACK_IMPORTED_MODULE_1_vue__["a" /* default */].http.interceptors.push((request, next) => {
    console.log(localStorage.getItem('token'));
    request.headers.set('Authorization', localStorage.getItem('token'));
    request.headers.set('Accept', 'application/json');
    next();
});

/* harmony default export */ __webpack_exports__["default"] = ({
    name: 'dashboard',
    data() {
        return {
            name: localStorage.getItem('name')
        };
    },
    methods: {
        logout() {
            localStorage.clear();
            this.$router.push({
                path: '/'
            });
        }
    }
});

/***/ }),
/* 22 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_sweetalert__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_sweetalert___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_sweetalert__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_vue__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_vue_router__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_vue_resource__ = __webpack_require__(3);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//





__WEBPACK_IMPORTED_MODULE_1_vue__["a" /* default */].use(__WEBPACK_IMPORTED_MODULE_3_vue_resource__["a" /* default */]);
__WEBPACK_IMPORTED_MODULE_1_vue__["a" /* default */].use(__WEBPACK_IMPORTED_MODULE_2_vue_router__["a" /* default */]);
/* harmony default export */ __webpack_exports__["default"] = ({
    name: 'documentation',
    data() {
        return {
            //service details
            name: '',
            status: '',
            description: '',
            APIs: [],
            seen: true,
            show: true,
            sid: ''
        };
    },
    methods: {
        getAllservices() {
            var id = this.$route.params.id;
            console.log("+++++++" + globalLocalHost);
            this.$http.get(globalLocalHost + '/getDocumentation/' + id).then(response => {
                if (response.body.err == false) {
                    //sweetalert('Success!', 'Login successful', 'success');
                    console.log(response);
                    this.APIs = response.body.result.user;
                } else {
                    __WEBPACK_IMPORTED_MODULE_0_sweetalert___default()('Oops...', 'Something went wrong!', 'error');
                }
            }, response => {
                console.log(response);
                __WEBPACK_IMPORTED_MODULE_0_sweetalert___default()('Oops...', 'Something went wrong!', 'error');
            });
        },
        addAPI() {
            var id = this.$route.params.id;
            this.$router.push('/dashboard/addAPI/' + id);
        },
        deleteservice(service) {
            var pid = this.$route.params.id;
            this.sid = service.id;
            console.log("service id:" + this.sid);
            //  debugger
            console.log("+++++++" + globalLocalHost);
            this.$http.delete(globalLocalHost + '/user/deleteProjectService/' + pid + '/' + this.sid).then(response => {
                if (response.body.err == false) {
                    console.log(response);
                    this.services.splice(this.services.indexOf(service), 1);
                    __WEBPACK_IMPORTED_MODULE_0_sweetalert___default()('Success!', 'service deleted  successfully', 'success');
                    console.log("deleted");
                } else {
                    console.log(response);
                    __WEBPACK_IMPORTED_MODULE_0_sweetalert___default()('Oops...', 'Something went wrong!', 'error');
                }
            }, response => {
                //  sweetalert('Oops...', 'Something went wrong!', 'error')
                console.log(response);
            });
        },
        div_show1(service) {
            this.sid = service.id;
            console.log("here");
            document.getElementById('frm1').style.display = "block";
            document.getElementById('tb').style.display = "none";
            //add values to edit form
            this.name = service.name, this.status = service.status, this.description = service.description;
        },
        div_hide1() {
            document.getElementById('frm1').style.display = "none";
            document.getElementById('tb').style.display = "block";
            console.log("call edit button");
            //create json for update api
            var obj = {
                'service': {
                    'name': this.name,
                    'description': this.description,
                    'status': this.status
                }
                //call edit api
            };var pid = this.$route.params.id;
            this.userid = localStorage.getItem('userId');
            console.log("services:" + pid);
            console.log("ownerId:" + this.userid);
            console.log("+++++++" + globalLocalHost);
            this.$http.put(globalLocalHost + '/user/updateProjectService/' + pid + '/' + this.sid, obj).then(response => {
                if (response.body.err == false) {
                    console.log(response);
                    this.getAllservices();
                    __WEBPACK_IMPORTED_MODULE_0_sweetalert___default()('Success!', 'updated seccessfully done', 'success');
                } else {
                    console.log(response);
                    __WEBPACK_IMPORTED_MODULE_0_sweetalert___default()('Oops...', 'Something went wrong!', 'error');
                }
            }, response => {
                __WEBPACK_IMPORTED_MODULE_0_sweetalert___default()('Oops...', 'Something went wrong!', 'error');
            });
        },
        documentation(api) {
            this.$router.push('/dashboard/documentation/' + api._id);
        },
        cancelop() {
            // alert('hello');
            // debugger
            var id = localStorage.getItem('ser_id');
            localStorage.removeItem('ser_id');
            this.$router.push('/dashboard/api/' + id);
        }
    },
    created() {
        this.getAllservices();
    }
});

/***/ }),
/* 23 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_sweetalert__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_sweetalert___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_sweetalert__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_vue__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_vue_resource__ = __webpack_require__(3);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




__WEBPACK_IMPORTED_MODULE_1_vue__["a" /* default */].use(__WEBPACK_IMPORTED_MODULE_2_vue_resource__["a" /* default */]);

/* harmony default export */ __webpack_exports__["default"] = ({
    name: 'index',
    data() {
        return {
            projectCOunt: '',
            ServiceCount: '',
            apicount: ''
        };
    },
    methods: {
        getProjectsCount() {
            console.log("ipaddress:" + globalLocalHost);
            this.$http.get(globalLocalHost + '/getcountOfProject').then(response => {
                if (response.body.err == false) {
                    //sweetalert('Success!', 'Login successful', 'success');
                    console.log(response);
                    this.projectCOunt = response.body.result.project;
                } else {
                    __WEBPACK_IMPORTED_MODULE_0_sweetalert___default()('Oops...', 'Something went wrong!', 'error');
                }
            }, response => {
                console.log(response);
                __WEBPACK_IMPORTED_MODULE_0_sweetalert___default()('Oops...', 'Something went wrong!', 'error');
            });
        },
        getApiCount() {
            console.log("ipaddress:" + globalLocalHost);
            this.$http.get(globalLocalHost + '/getCountOfDocumentation').then(response => {
                if (response.body.err == false) {
                    //sweetalert('Success!', 'Login successful', 'success');
                    console.log(response);
                    this.apicount = response.body.result.documentaion;
                } else {
                    __WEBPACK_IMPORTED_MODULE_0_sweetalert___default()('Oops...', 'Something went wrong!', 'error');
                }
            }, response => {
                console.log(response);
                __WEBPACK_IMPORTED_MODULE_0_sweetalert___default()('Oops...', 'Something went wrong!', 'error');
            });
        }
    },
    created() {
        this.getProjectsCount(), this.getApiCount();
    }
});

/***/ }),
/* 24 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_sweetalert__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_sweetalert___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_sweetalert__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_vue__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_vue_router__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_vue_resource__ = __webpack_require__(3);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//





__WEBPACK_IMPORTED_MODULE_1_vue__["a" /* default */].use(__WEBPACK_IMPORTED_MODULE_3_vue_resource__["a" /* default */]);
__WEBPACK_IMPORTED_MODULE_1_vue__["a" /* default */].use(__WEBPACK_IMPORTED_MODULE_2_vue_router__["a" /* default */]);
/* harmony default export */ __webpack_exports__["default"] = ({
    name: 'login',
    data() {
        return {
            email: '',
            password: '',
            userid: '',
            token: ''
        };
    },
    methods: {
        login() {
            var obj = {
                'user': {
                    'email': this.email,
                    'password': this.password
                }
                //added globalLocalHost value to change it change from global.js
            };console.log("+++++++" + globalLocalHost);
            this.$http.post(globalLocalHost + '/userLogin', obj).then(response => {
                if (response.body.err == false) {
                    console.log(response);
                    this.userid = response.body.result.id;
                    this.token = response.body.result.token;
                    this.name = response.body.result.name;

                    console.log("********************" + this.userid, this.token, this.name);
                    localStorage.setItem('name', this.name);
                    localStorage.setItem('userId', this.userid);
                    localStorage.setItem('token', this.token);
                    //sweetalert('Success!', 'Login successful', 'success');
                    this.$router.push({
                        path: '/dashboard/index'

                    });
                } else {
                    console.log(response);
                    __WEBPACK_IMPORTED_MODULE_0_sweetalert___default()('Oops...', 'Something went wrong!', 'error');
                }
            }, response => {
                __WEBPACK_IMPORTED_MODULE_0_sweetalert___default()('Oops...', 'Something went wrong!', 'error');
            });
        }
    }
});

/***/ }),
/* 25 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_sweetalert__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_sweetalert___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_sweetalert__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_vue__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_vue_router__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_vue_resource__ = __webpack_require__(3);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//






__WEBPACK_IMPORTED_MODULE_1_vue__["a" /* default */].use(__WEBPACK_IMPORTED_MODULE_3_vue_resource__["a" /* default */]);
__WEBPACK_IMPORTED_MODULE_1_vue__["a" /* default */].use(__WEBPACK_IMPORTED_MODULE_2_vue_router__["a" /* default */]);
/* harmony default export */ __webpack_exports__["default"] = ({
    name: 'login',
    data() {
        return {
            //service details
            name: '',
            status: '',
            description: '',
            services: [],
            seen: true,
            show: true,
            sid: ''
        };
    },
    methods: {
        getAllservices() {
            var id = this.$route.params.id;
            console.log("IpAddress:" + globalLocalHost);
            this.$http.get(globalLocalHost + '/user/getProjectAllServies/' + id).then(response => {
                if (response.body.err == false) {
                    //sweetalert('Success!', 'Login successful', 'success');
                    console.log(response);
                    this.services = response.body.result.services;
                } else {
                    __WEBPACK_IMPORTED_MODULE_0_sweetalert___default()('Oops...', 'Something went wrong!', 'error');
                }
            }, response => {
                console.log(response);
            });
        },
        addservice() {
            var id = this.$route.params.id;
            this.$router.push('/dashboard/addservice/' + id);
        },
        deleteservice(service) {
            var pid = this.$route.params.id;
            this.sid = service.id;
            console.log("service id:" + this.sid);
            console.log("IpAddress:" + globalLocalHost);
            this.$http.delete(globalLocalHost + '/user/deleteProjectService/' + pid + '/' + this.sid).then(response => {
                if (response.body.err == false) {
                    console.log(response);
                    this.services.splice(this.services.indexOf(service), 1);
                    __WEBPACK_IMPORTED_MODULE_0_sweetalert___default()('Success!', 'service deleted  successfully', 'success');
                    console.log("deleted");
                } else {
                    console.log(response);
                    __WEBPACK_IMPORTED_MODULE_0_sweetalert___default()('Oops...', 'Something went wrong!', 'error');
                }
            }, response => {
                __WEBPACK_IMPORTED_MODULE_0_sweetalert___default()('Oops...', 'Something went wrong!', 'error');
                console.log(response);
            });
        },
        div_show1(service) {
            this.sid = service.id;
            console.log("here");
            document.getElementById('frm1').style.display = "block";
            document.getElementById('tb').style.display = "none";
            //add values to edit form
            this.name = service.name, this.status = service.status, this.description = service.description;
        },
        div_hide1() {
            document.getElementById('frm1').style.display = "none";
            document.getElementById('tb').style.display = "block";
            console.log("call edit button");
            //create json for update api
            var obj = {
                'service': {
                    'name': this.name,
                    'description': this.description,
                    'status': this.status
                }
                //call edit api
            };var pid = this.$route.params.id;
            this.userid = localStorage.getItem('userId');
            console.log("services:" + pid);
            console.log("ownerId:" + this.userid);
            console.log("IpAddress:" + globalLocalHost);
            this.$http.put(globalLocalHost + '/user/updateProjectService/' + pid + '/' + this.sid, obj).then(response => {
                if (response.body.err == false) {
                    console.log(response);
                    this.getAllservices();
                    __WEBPACK_IMPORTED_MODULE_0_sweetalert___default()('Success!', 'updated seccessfully done', 'success');
                } else {
                    console.log(response);
                    __WEBPACK_IMPORTED_MODULE_0_sweetalert___default()('Oops...', 'Something went wrong!', 'error');
                }
            }, response => {
                __WEBPACK_IMPORTED_MODULE_0_sweetalert___default()('Oops...', 'Something went wrong!', 'error');
            });
        },
        api(service) {
            this.$router.push('/dashboard/api/' + service.id);
        },
        cancelop() {
            document.getElementById('frm1').style.display = "none";
            document.getElementById('tb').style.display = "block";
        }
    },

    created() {
        this.getAllservices();
    }
});

/***/ }),
/* 26 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_sweetalert__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_sweetalert___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_sweetalert__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_vue__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_vue_resource__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_vue_router__ = __webpack_require__(4);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//





__WEBPACK_IMPORTED_MODULE_1_vue__["a" /* default */].use(__WEBPACK_IMPORTED_MODULE_2_vue_resource__["a" /* default */]);
__WEBPACK_IMPORTED_MODULE_1_vue__["a" /* default */].use(__WEBPACK_IMPORTED_MODULE_3_vue_router__["a" /* default */]);
/* harmony default export */ __webpack_exports__["default"] = ({
    name: 'signup',
    data() {
        return {
            email: '',
            password: '',
            firstName: '',
            lastName: '',
            password: '',
            userrole: '',
            username: '',
            status: '',
            usertype: '',
            mobile: ''
        };
    },
    methods: {
        signup() {
            var obj = {
                'user': {
                    'firstName': this.firstName,
                    'lastName': this.lastName,
                    'username': this.username,
                    'email': this.email,
                    'password': this.password,
                    'userrole': this.userrole,
                    'status': this.status,
                    'usertype': 'user',
                    'mobile': this.mobile
                }
                //added globalLocalHost value to change it change from global.js
            };console.log("+++" + globalLocalHost);
            this.$http.post(globalLocalHost + '/addUser', obj).then(response => {
                if (response.body.err === false) {
                    __WEBPACK_IMPORTED_MODULE_0_sweetalert___default()('Success!', 'usser added successsfully', 'success');
                    this.$router.push({
                        path: '/'
                    });
                } else {
                    console.log(response);
                    __WEBPACK_IMPORTED_MODULE_0_sweetalert___default()('Oops...', 'Something went wrong! email and mobile already exist', 'error');
                }
            }, response => {});
        }
    }
});

/***/ }),
/* 27 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__App__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__App___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__App__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__router__ = __webpack_require__(10);
// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.




__WEBPACK_IMPORTED_MODULE_0_vue__["a" /* default */].config.productionTip = false;

/* eslint-disable no-new */
new __WEBPACK_IMPORTED_MODULE_0_vue__["a" /* default */]({
    el: '#app',
    router: __WEBPACK_IMPORTED_MODULE_2__router__["a" /* default */],
    template: '<App/>',
    components: { App: __WEBPACK_IMPORTED_MODULE_1__App___default.a }
});

/***/ }),
/* 28 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 29 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 30 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 31 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 32 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 33 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 34 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 35 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 36 */,
/* 37 */,
/* 38 */,
/* 39 */,
/* 40 */,
/* 41 */,
/* 42 */,
/* 43 */
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(31)

var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(13),
  /* template */
  __webpack_require__(65),
  /* scopeId */
  "data-v-55f97949",
  /* cssModules */
  null
)

module.exports = Component.exports


/***/ }),
/* 44 */
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(32)

var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(14),
  /* template */
  __webpack_require__(66),
  /* scopeId */
  null,
  /* cssModules */
  null
)

module.exports = Component.exports


/***/ }),
/* 45 */
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(15),
  /* template */
  __webpack_require__(70),
  /* scopeId */
  null,
  /* cssModules */
  null
)

module.exports = Component.exports


/***/ }),
/* 46 */
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(16),
  /* template */
  __webpack_require__(58),
  /* scopeId */
  null,
  /* cssModules */
  null
)

module.exports = Component.exports


/***/ }),
/* 47 */
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(17),
  /* template */
  __webpack_require__(69),
  /* scopeId */
  null,
  /* cssModules */
  null
)

module.exports = Component.exports


/***/ }),
/* 48 */
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(35)

var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(18),
  /* template */
  __webpack_require__(73),
  /* scopeId */
  null,
  /* cssModules */
  null
)

module.exports = Component.exports


/***/ }),
/* 49 */
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(30)

var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(19),
  /* template */
  __webpack_require__(62),
  /* scopeId */
  null,
  /* cssModules */
  null
)

module.exports = Component.exports


/***/ }),
/* 50 */
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(33)

var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(20),
  /* template */
  __webpack_require__(67),
  /* scopeId */
  null,
  /* cssModules */
  null
)

module.exports = Component.exports


/***/ }),
/* 51 */
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(21),
  /* template */
  __webpack_require__(60),
  /* scopeId */
  null,
  /* cssModules */
  null
)

module.exports = Component.exports


/***/ }),
/* 52 */
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(34)

var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(22),
  /* template */
  __webpack_require__(71),
  /* scopeId */
  "data-v-860cff5e",
  /* cssModules */
  null
)

module.exports = Component.exports


/***/ }),
/* 53 */
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(23),
  /* template */
  __webpack_require__(68),
  /* scopeId */
  null,
  /* cssModules */
  null
)

module.exports = Component.exports


/***/ }),
/* 54 */
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(24),
  /* template */
  __webpack_require__(63),
  /* scopeId */
  null,
  /* cssModules */
  null
)

module.exports = Component.exports


/***/ }),
/* 55 */
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(0)(
  /* script */
  null,
  /* template */
  __webpack_require__(72),
  /* scopeId */
  null,
  /* cssModules */
  null
)

module.exports = Component.exports


/***/ }),
/* 56 */
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(29)

var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(25),
  /* template */
  __webpack_require__(61),
  /* scopeId */
  null,
  /* cssModules */
  null
)

module.exports = Component.exports


/***/ }),
/* 57 */
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(26),
  /* template */
  __webpack_require__(64),
  /* scopeId */
  null,
  /* cssModules */
  null
)

module.exports = Component.exports


/***/ }),
/* 58 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "content"
  }, [_c('div', {
    staticClass: "container-fluid"
  }, [_c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-8"
  }, [_c('div', {
    staticClass: "card"
  }, [_vm._m(0), _vm._v(" "), _c('div', {
    staticClass: "card-content"
  }, [_c('form', [_c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-12"
  }, [_c('div', {
    staticClass: "form-group label-floating"
  }, [_c('label', {
    staticClass: "control-label",
    attrs: {
      "for": "name"
    }
  }, [_vm._v("name")]), _vm._v(" "), _c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.name),
      expression: "name"
    }],
    staticClass: "form-control",
    attrs: {
      "type": "text",
      "name": "name",
      "required": ""
    },
    domProps: {
      "value": (_vm.name)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.name = $event.target.value
      }
    }
  })])])]), _vm._v(" "), _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-6"
  }, [_c('div', {
    staticClass: "form-group label-floating"
  }, [_c('label', {
    staticClass: "control-label",
    attrs: {
      "for": "isPrivate"
    }
  }, [_vm._v("project acess")]), _vm._v(" "), _c('select', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.isPrivate),
      expression: "isPrivate"
    }],
    staticClass: "form-control",
    attrs: {
      "name": "isPrivate",
      "id": "isPrivate",
      "required": ""
    },
    on: {
      "change": function($event) {
        var $$selectedVal = Array.prototype.filter.call($event.target.options, function(o) {
          return o.selected
        }).map(function(o) {
          var val = "_value" in o ? o._value : o.value;
          return val
        });
        _vm.isPrivate = $event.target.multiple ? $$selectedVal : $$selectedVal[0]
      }
    }
  }, [_c('option', {
    attrs: {
      "value": "public"
    }
  }, [_vm._v("Public")]), _vm._v(" "), _c('option', {
    attrs: {
      "value": "private"
    }
  }, [_vm._v("Private")])])])]), _vm._v(" "), _c('div', {
    staticClass: "col-md-6"
  }, [_c('div', {
    staticClass: "form-group label-floating"
  }, [_c('label', {
    staticClass: "control-label",
    attrs: {
      "for": "status"
    }
  }, [_vm._v("status")]), _vm._v(" "), _c('select', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.status),
      expression: "status"
    }],
    staticClass: "form-control",
    attrs: {
      "name": "status",
      "id": "status",
      "required": ""
    },
    on: {
      "change": function($event) {
        var $$selectedVal = Array.prototype.filter.call($event.target.options, function(o) {
          return o.selected
        }).map(function(o) {
          var val = "_value" in o ? o._value : o.value;
          return val
        });
        _vm.status = $event.target.multiple ? $$selectedVal : $$selectedVal[0]
      }
    }
  }, [_c('option', {
    attrs: {
      "value": "active"
    }
  }, [_vm._v("Active")]), _vm._v(" "), _c('option', {
    attrs: {
      "value": "inactive"
    }
  }, [_vm._v("InActive")])])])])]), _vm._v(" "), _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-12"
  }, [_c('div', {
    staticClass: "form-group label-floating"
  }, [_c('label', {
    staticClass: "control-label",
    attrs: {
      "for": "purpose"
    }
  }, [_vm._v("Purpose")]), _vm._v(" "), _c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.purpose),
      expression: "purpose"
    }],
    staticClass: "form-control",
    attrs: {
      "type": "text",
      "name": "purpose",
      "required": ""
    },
    domProps: {
      "value": (_vm.purpose)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.purpose = $event.target.value
      }
    }
  })])])]), _vm._v(" "), _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-12"
  }, [_c('div', {
    staticClass: "form-group label-floating"
  }, [_c('label', {
    staticClass: "control-label",
    attrs: {
      "for": "authenticationDescription"
    }
  }, [_vm._v("Authentication Description")]), _vm._v(" "), _c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.authenticationDescription),
      expression: "authenticationDescription"
    }],
    staticClass: "form-control",
    attrs: {
      "type": "text",
      "name": "authenticationDescription",
      "required": ""
    },
    domProps: {
      "value": (_vm.authenticationDescription)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.authenticationDescription = $event.target.value
      }
    }
  })])])]), _vm._v(" "), _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticStyle: {
      "margin-left": "30%"
    }
  }, [_c('button', {
    staticClass: "btn btn-primary btn-round",
    attrs: {
      "type": "reset"
    },
    on: {
      "click": function($event) {
        _vm.cancelop()
      }
    }
  }, [_vm._v("Cancel")]), _vm._v(" "), _c('button', {
    staticClass: "btn btn-primary btn-round",
    attrs: {
      "type": "button"
    },
    on: {
      "click": function($event) {
        _vm.add()
      }
    }
  }, [_vm._v("Submit")])])]), _vm._v(" "), _c('div', {
    staticClass: "clearfix"
  })])])])])])])])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "card-header",
    attrs: {
      "data-background-color": "purple"
    }
  }, [_c('h4', {
    staticClass: "title"
  }, [_vm._v(" Add Project")]), _vm._v(" "), _c('p', {
    staticClass: "category"
  }, [_vm._v("Add Project Information")])])
}]}

/***/ }),
/* 59 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    attrs: {
      "id": "app"
    }
  }, [_c('router-view')], 1)
},staticRenderFns: []}

/***/ }),
/* 60 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "wrapper"
  }, [_c('div', {
    staticClass: "sidebar",
    attrs: {
      "data-color": "purple",
      "data-image": "../../static/img/sidebar-1.jpg"
    }
  }, [_vm._m(0), _vm._v(" "), _c('div', {
    staticClass: "sidebar-wrapper"
  }, [_c('ul', {
    staticClass: "nav"
  }, [_vm._m(1), _vm._v(" "), _vm._m(2), _vm._v(" "), _vm._m(3), _vm._v(" "), _c('li', [_c('a', {
    on: {
      "click": function($event) {
        _vm.logout()
      }
    }
  }, [_c('i', {
    staticClass: "material-icons"
  }, [_vm._v("settings_power")]), _vm._v(" "), _c('p', [_vm._v("Logout")])])]), _vm._v(" "), _vm._m(4)])])]), _vm._v(" "), _c('div', {
    staticClass: "main-panel"
  }, [_c('nav', {
    staticClass: "navbar navbar-transparent navbar-absolute"
  }, [_c('div', {
    staticClass: "container-fluid"
  }, [_vm._m(5), _vm._v(" "), _c('div', {
    staticClass: "collapse navbar-collapse"
  }, [_c('ul', {
    staticClass: "nav navbar-nav navbar-right"
  }, [_c('li', [_c('a', {
    staticClass: "dropdown-toggle",
    attrs: {
      "href": "#pablo",
      "data-toggle": "dropdown"
    }
  }, [_c('p', [_c('i', {
    staticClass: "material-icons"
  }, [_vm._v("person")]), _vm._v("ACCOUNT:"), _c('b', [_vm._v(_vm._s(_vm.name))])])])])])])])]), _vm._v(" "), _c('div', {
    staticClass: "content"
  }, [_c('div', {
    staticClass: "container-fluid"
  }, [_c('router-view')], 1)])])])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "logo"
  }, [_c('a', {
    staticClass: "simple-text",
    attrs: {
      "href": "/#/dashboard"
    }
  }, [_vm._v("\n\t\t\t\tAPI Planner\n\t\t\t\t")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('li', {
    staticClass: "active"
  }, [_c('a', {
    attrs: {
      "href": "/#/dashboard/index"
    }
  }, [_c('i', {
    staticClass: "material-icons"
  }, [_vm._v("dashboard")]), _vm._v(" "), _c('p', [_vm._v("Dashboard")])])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('li', [_c('a', {
    attrs: {
      "href": "/#/dashboard/allprojects"
    }
  }, [_c('i', {
    staticClass: "material-icons"
  }, [_vm._v("content_paste")]), _vm._v(" "), _c('p', [_vm._v("All Projects")])])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('li', [_c('a', {
    staticClass: "disabled"
  }, [_c('i', {
    staticClass: "material-icons"
  }, [_vm._v("import_export")]), _vm._v(" "), _c('p', [_vm._v("Exports")])])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('li', [_c('a', {
    attrs: {
      "href": "/#/dashboard/adduser"
    }
  }, [_c('i', {
    staticClass: "material-icons"
  }, [_vm._v("")]), _vm._v(" "), _c('p', [_vm._v("Add User")])])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "navbar-header"
  }, [_c('button', {
    staticClass: "navbar-toggle",
    attrs: {
      "type": "button",
      "data-toggle": "collapse"
    }
  }, [_c('span', {
    staticClass: "sr-only"
  }, [_vm._v("Toggle navigation")]), _vm._v(" "), _c('span', {
    staticClass: "icon-bar"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-bar"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-bar"
  })]), _vm._v(" "), _c('a', {
    staticClass: "navbar-brand",
    attrs: {
      "href": "#"
    }
  }, [_vm._v("API Planner")])])
}]}

/***/ }),
/* 61 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "content"
  }, [_c('div', {
    staticClass: "container-fluid",
    attrs: {
      "id": "tb"
    }
  }, [_c('div', {
    staticClass: "row"
  }, [_c('input', {
    staticClass: "btn btn-md btn-success",
    attrs: {
      "type": "button",
      "value": "Add New Service"
    },
    on: {
      "click": function($event) {
        _vm.addservice()
      }
    }
  }), _vm._v(" "), _c('div', {
    staticClass: "col-md-12"
  }, [_c('div', {
    staticClass: "card"
  }, [_vm._m(0), _vm._v(" "), _c('div', {
    staticClass: "card-content table-responsive"
  }, [_c('table', {
    staticClass: "table table-hover"
  }, [_vm._m(1), _vm._v(" "), _c('tbody', _vm._l((_vm.services), function(service) {
    return _c('tr', [_c('td', [_vm._v(_vm._s(service.name))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(service.description))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(service.status))]), _vm._v(" "), _c('td', [_c('input', {
      staticClass: "btn btn-sm  btn-danger",
      attrs: {
        "type": "button",
        "value": "Delete"
      },
      on: {
        "click": function($event) {
          _vm.deleteservice(service)
        }
      }
    }), _vm._v(" "), _c('input', {
      staticClass: "btn btn-sm  btn-primary",
      attrs: {
        "type": "button",
        "value": "Edit"
      },
      on: {
        "click": function($event) {
          _vm.div_show1(service)
        }
      }
    })]), _vm._v(" "), _c('td', [_c('input', {
      staticClass: "btn btn-sm btn-info",
      attrs: {
        "type": "button",
        "name": "",
        "value": "click to See API's"
      },
      on: {
        "click": function($event) {
          _vm.api(service)
        }
      }
    })])])
  }))])])])])])]), _vm._v(" "), _c('div', {
    staticStyle: {
      "display": "none"
    },
    attrs: {
      "id": "frm1"
    }
  }, [_c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-8"
  }, [_c('div', {
    staticClass: "card"
  }, [_vm._m(2), _vm._v(" "), _c('div', {
    staticClass: "card-content"
  }, [_c('form', [_c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-12"
  }, [_c('div', {
    staticClass: "form-group label-floating"
  }, [_c('label', {
    staticClass: "control-label",
    attrs: {
      "for": "name"
    }
  }, [_vm._v("name")]), _vm._v(" "), _c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.name),
      expression: "name"
    }],
    staticClass: "form-control",
    attrs: {
      "type": "text",
      "name": "name",
      "required": ""
    },
    domProps: {
      "value": (_vm.name)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.name = $event.target.value
      }
    }
  })])])]), _vm._v(" "), _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-6"
  }, [_c('div', {
    staticClass: "form-group label-floating"
  }, [_c('label', {
    staticClass: "control-label",
    attrs: {
      "for": "status"
    }
  }, [_vm._v("status")]), _vm._v(" "), _c('select', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.status),
      expression: "status"
    }],
    staticClass: "form-control",
    attrs: {
      "name": "status",
      "id": "status",
      "required": ""
    },
    on: {
      "change": function($event) {
        var $$selectedVal = Array.prototype.filter.call($event.target.options, function(o) {
          return o.selected
        }).map(function(o) {
          var val = "_value" in o ? o._value : o.value;
          return val
        });
        _vm.status = $event.target.multiple ? $$selectedVal : $$selectedVal[0]
      }
    }
  }, [_c('option', {
    attrs: {
      "value": "active"
    }
  }, [_vm._v("Active")]), _vm._v(" "), _c('option', {
    attrs: {
      "value": "inactive"
    }
  }, [_vm._v("InActive")])])])])]), _vm._v(" "), _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-12"
  }, [_c('div', {
    staticClass: "form-group label-floating"
  }, [_c('label', {
    staticClass: "control-label",
    attrs: {
      "for": "description"
    }
  }, [_vm._v(" Description")]), _vm._v(" "), _c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.description),
      expression: "description"
    }],
    staticClass: "form-control",
    attrs: {
      "type": "text",
      "name": "description",
      "required": ""
    },
    domProps: {
      "value": (_vm.description)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.description = $event.target.value
      }
    }
  })])])]), _vm._v(" "), _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticStyle: {
      "margin-left": "30%"
    }
  }, [_c('button', {
    staticClass: "btn btn-primary btn-round",
    attrs: {
      "type": "reset"
    },
    on: {
      "click": function($event) {
        _vm.cancelop()
      }
    }
  }, [_vm._v("Cancel")]), _vm._v(" "), _c('button', {
    staticClass: "btn btn-primary btn-round",
    attrs: {
      "type": "button"
    },
    on: {
      "click": function($event) {
        _vm.div_hide1()
      }
    }
  }, [_vm._v("Submit")])])]), _vm._v(" "), _c('div', {
    staticClass: "clearfix"
  })])])])])])])])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "card-header",
    attrs: {
      "data-background-color": "purple"
    }
  }, [_c('h4', {
    staticClass: "title"
  }, [_vm._v("Service List")]), _vm._v(" "), _c('p', {
    staticClass: "category"
  }, [_vm._v("Service list of project  ")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('thead', [_c('th', [_vm._v("Name")]), _vm._v(" "), _c('th', [_vm._v("Description")]), _vm._v(" "), _c('th', [_vm._v("Status")]), _vm._v(" "), _c('th', [_vm._v("Actions")]), _vm._v(" "), _c('th', [_vm._v("API's")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "card-header",
    attrs: {
      "data-background-color": "purple"
    }
  }, [_c('h4', {
    staticClass: "title"
  }, [_vm._v(" Edit service")]), _vm._v(" "), _c('p', {
    staticClass: "category"
  }, [_vm._v("Add service Information")])])
}]}

/***/ }),
/* 62 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "content"
  }, [_c('div', {
    staticClass: "container-fluid",
    attrs: {
      "id": "tb"
    }
  }, [_c('div', {
    staticClass: "row"
  }, [_c('input', {
    staticClass: "btn btn-md btn-success",
    staticStyle: {
      "background-color": "#9c27b0",
      "color": "#FFFFFF"
    },
    attrs: {
      "type": "button",
      "value": "Add New Project"
    },
    on: {
      "click": function($event) {
        _vm.addproject()
      }
    }
  }), _vm._v(" "), _c('div', {
    staticClass: "col-md-12"
  }, [_c('div', {
    staticClass: "card"
  }, [_vm._m(0), _vm._v(" "), _c('div', {
    staticClass: "card-content table-responsive"
  }, [_c('table', {
    staticClass: "table table-hover"
  }, [_vm._m(1), _vm._v(" "), _c('tbody', _vm._l((_vm.projects), function(project) {
    return _c('tr', [_c('td', [_vm._v(_vm._s(project.name))]), _vm._v(" "), _c('td', [_c('input', {
      staticClass: "btn btn-sm btn-info",
      attrs: {
        "type": "button",
        "value": "Services"
      },
      on: {
        "click": function($event) {
          _vm.services(project)
        }
      }
    })]), _vm._v(" "), _c('td', [_c('input', {
      staticClass: "btn btn-sm  btn-danger",
      attrs: {
        "type": "button",
        "value": "Delete"
      },
      on: {
        "click": function($event) {
          _vm.deleteproject(project)
        }
      }
    }), _vm._v(" "), _c('input', {
      staticClass: "btn btn-sm  btn-primary",
      attrs: {
        "type": "button",
        "value": "Edit"
      },
      on: {
        "click": function($event) {
          _vm.div_show1(project)
        }
      }
    }), _vm._v(" "), _c('input', {
      staticClass: "btn btn-sm  btn-primary",
      attrs: {
        "type": "button",
        "value": "Exports"
      },
      on: {
        "click": function($event) {
          _vm.exportprj(project)
        }
      }
    })])])
  }))])])])])])]), _vm._v(" "), _c('div', {
    staticStyle: {
      "display": "none"
    },
    attrs: {
      "id": "frm1"
    }
  }, [_c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-8"
  }, [_c('div', {
    staticClass: "card"
  }, [_vm._m(2), _vm._v(" "), _c('div', {
    staticClass: "card-content"
  }, [_c('form', [_c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-12"
  }, [_c('div', {
    staticClass: "form-group label-floating"
  }, [_c('label', {
    staticClass: "control-label",
    attrs: {
      "for": "name"
    }
  }, [_vm._v("name")]), _vm._v(" "), _c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.name),
      expression: "name"
    }],
    staticClass: "form-control",
    attrs: {
      "type": "text",
      "name": "name",
      "required": ""
    },
    domProps: {
      "value": (_vm.name)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.name = $event.target.value
      }
    }
  })])])]), _vm._v(" "), _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-6"
  }, [_c('div', {
    staticClass: "form-group label-floating"
  }, [_c('label', {
    staticClass: "control-label",
    attrs: {
      "for": "isPrivate"
    }
  }, [_vm._v("project acess")]), _vm._v(" "), _c('select', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.isPrivate),
      expression: "isPrivate"
    }],
    staticClass: "form-control",
    attrs: {
      "name": "isPrivate",
      "id": "isPrivate",
      "required": ""
    },
    on: {
      "change": function($event) {
        var $$selectedVal = Array.prototype.filter.call($event.target.options, function(o) {
          return o.selected
        }).map(function(o) {
          var val = "_value" in o ? o._value : o.value;
          return val
        });
        _vm.isPrivate = $event.target.multiple ? $$selectedVal : $$selectedVal[0]
      }
    }
  }, [_c('option', {
    attrs: {
      "value": "public"
    }
  }, [_vm._v("Public")]), _vm._v(" "), _c('option', {
    attrs: {
      "value": "private"
    }
  }, [_vm._v("Private")])])])]), _vm._v(" "), _c('div', {
    staticClass: "col-md-6"
  }, [_c('div', {
    staticClass: "form-group label-floating"
  }, [_c('label', {
    staticClass: "control-label",
    attrs: {
      "for": "status"
    }
  }, [_vm._v("status")]), _vm._v(" "), _c('select', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.status),
      expression: "status"
    }],
    staticClass: "form-control",
    attrs: {
      "name": "status",
      "id": "status",
      "required": ""
    },
    on: {
      "change": function($event) {
        var $$selectedVal = Array.prototype.filter.call($event.target.options, function(o) {
          return o.selected
        }).map(function(o) {
          var val = "_value" in o ? o._value : o.value;
          return val
        });
        _vm.status = $event.target.multiple ? $$selectedVal : $$selectedVal[0]
      }
    }
  }, [_c('option', {
    attrs: {
      "value": "active"
    }
  }, [_vm._v("Active")]), _vm._v(" "), _c('option', {
    attrs: {
      "value": "inactive"
    }
  }, [_vm._v("InActive")])])])])]), _vm._v(" "), _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-12"
  }, [_c('div', {
    staticClass: "form-group label-floating"
  }, [_c('label', {
    staticClass: "control-label",
    attrs: {
      "for": "purpose"
    }
  }, [_vm._v("Purpose")]), _vm._v(" "), _c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.purpose),
      expression: "purpose"
    }],
    staticClass: "form-control",
    attrs: {
      "type": "text",
      "name": "purpose",
      "required": ""
    },
    domProps: {
      "value": (_vm.purpose)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.purpose = $event.target.value
      }
    }
  })])])]), _vm._v(" "), _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-12"
  }, [_c('div', {
    staticClass: "form-group label-floating"
  }, [_c('label', {
    staticClass: "control-label",
    attrs: {
      "for": "authenticationDescription"
    }
  }, [_vm._v("Authentication Description")]), _vm._v(" "), _c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.authenticationDescription),
      expression: "authenticationDescription"
    }],
    staticClass: "form-control",
    attrs: {
      "type": "text",
      "name": "authenticationDescription",
      "required": ""
    },
    domProps: {
      "value": (_vm.authenticationDescription)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.authenticationDescription = $event.target.value
      }
    }
  })])])]), _vm._v(" "), _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticStyle: {
      "margin-left": "30%"
    }
  }, [_c('button', {
    staticClass: "btn btn-primary btn-round",
    attrs: {
      "type": "reset"
    },
    on: {
      "click": function($event) {
        _vm.cancelop()
      }
    }
  }, [_vm._v("Cancel")]), _vm._v(" "), _c('button', {
    staticClass: "btn btn-primary btn-round",
    attrs: {
      "type": "button"
    },
    on: {
      "click": function($event) {
        _vm.div_hide1()
      }
    }
  }, [_vm._v("Submit")])])]), _vm._v(" "), _c('div', {
    staticClass: "clearfix"
  })])])])])])])])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "card-header",
    attrs: {
      "data-background-color": "purple"
    }
  }, [_c('h4', {
    staticClass: "title"
  }, [_vm._v("PROJECT LIST")]), _vm._v(" "), _c('p', {
    staticClass: "category"
  }, [_vm._v("Following is the Project list with its services and controls     ")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('thead', [_c('th', [_vm._v("Name")]), _vm._v(" "), _c('th', [_vm._v("Services")]), _vm._v(" "), _c('th', [_vm._v("Contrlos")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "card-header",
    attrs: {
      "data-background-color": "purple"
    }
  }, [_c('h4', {
    staticClass: "title"
  }, [_vm._v(" Edit Project")]), _vm._v(" "), _c('p', {
    staticClass: "category"
  }, [_vm._v("Edit Project Information")])])
}]}

/***/ }),
/* 63 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', [_vm._m(0), _vm._v(" "), _vm._m(1), _vm._v(" "), _c('div', {
    staticClass: "container"
  }, [_c('div', {
    staticClass: "content"
  }, [_c('div', {
    staticClass: "container-fluid"
  }, [_c('div', {
    staticClass: "row",
    staticStyle: {
      "margin-left": "200px",
      "margin-top": "180px"
    }
  }, [_c('div', {
    staticClass: "col-md-8"
  }, [_c('div', {
    staticClass: "card"
  }, [_vm._m(2), _vm._v(" "), _c('div', {
    staticClass: "card-content"
  }, [_c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-10"
  }, [_c('div', {
    staticClass: "form-group label-floating"
  }, [_c('label', {
    staticClass: "control-label"
  }, [_vm._v("Email address")]), _vm._v(" "), _c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.email),
      expression: "email"
    }],
    staticClass: "form-control",
    attrs: {
      "type": "email",
      "name": "email",
      "id": "email"
    },
    domProps: {
      "value": (_vm.email)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.email = $event.target.value
      }
    }
  })])])]), _vm._v(" "), _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-10"
  }, [_c('div', {
    staticClass: "form-group label-floating"
  }, [_c('label', {
    staticClass: "control-label"
  }, [_vm._v("Password")]), _vm._v(" "), _c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.password),
      expression: "password"
    }],
    staticClass: "form-control",
    attrs: {
      "type": "password",
      "name": "password",
      "id": "password"
    },
    domProps: {
      "value": (_vm.password)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.password = $event.target.value
      }
    }
  })])])]), _vm._v(" "), _c('button', {
    staticClass: "btn btn-primary pull-right",
    staticStyle: {
      "margin-right": "50px"
    },
    attrs: {
      "type": "submit"
    },
    on: {
      "click": function($event) {
        _vm.login()
      }
    }
  }, [_c('b', [_vm._v("Login")])])])])])])])])])])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', [_c('h2', {
    staticStyle: {
      "line-height": "12px",
      "font-size": "20pt",
      "margin-top": "20px",
      "margin-left": "15px",
      "position": "absolute",
      "padding-top": "225px",
      "top": "0",
      "left": "0",
      "color": "purple"
    }
  }, [_c('b', [_vm._v("API PLANNER APP")])])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', [_c('img', {
    attrs: {
      "src": "/static/img/foxteam.jpg",
      "align": "left"
    }
  })])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "card-header",
    attrs: {
      "data-background-color": "purple"
    }
  }, [_c('h4', {
    staticClass: "title"
  }, [_c('b', [_vm._v(" Login ")])])])
}]}

/***/ }),
/* 64 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "container"
  }, [_vm._m(0), _vm._v(" "), _c('div', {
    staticClass: "content"
  }, [_c('div', {
    staticClass: "container-fluid"
  }, [_c('div', {
    staticClass: "row",
    staticStyle: {
      "margin-left": "200px",
      "margin-top": "130px"
    }
  }, [_c('div', {
    staticClass: "col-md-8"
  }, [_c('div', {
    staticClass: "card"
  }, [_vm._m(1), _vm._v(" "), _c('div', {
    staticClass: "card-content"
  }, [_c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-6"
  }, [_c('div', {
    staticClass: "form-group label-floating"
  }, [_c('label', {
    staticClass: "control-label"
  }, [_vm._v("Username")]), _vm._v(" "), _c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.username),
      expression: "username"
    }],
    staticClass: "form-control",
    attrs: {
      "type": "text",
      "name": "username",
      "id": "username",
      "required": ""
    },
    domProps: {
      "value": (_vm.username)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.username = $event.target.value
      }
    }
  })])]), _vm._v(" "), _c('div', {
    staticClass: "col-md-6"
  }, [_c('div', {
    staticClass: "form-group label-floating"
  }, [_c('label', {
    staticClass: "control-label"
  }, [_vm._v("Email address")]), _vm._v(" "), _c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.email),
      expression: "email"
    }],
    staticClass: "form-control",
    attrs: {
      "type": "email",
      "name": "email",
      "id": "email",
      "required": ""
    },
    domProps: {
      "value": (_vm.email)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.email = $event.target.value
      }
    }
  })])])]), _vm._v(" "), _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-6"
  }, [_c('div', {
    staticClass: "form-group label-floating"
  }, [_c('label', {
    staticClass: "control-label"
  }, [_vm._v("Fist Name")]), _vm._v(" "), _c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.firstName),
      expression: "firstName"
    }],
    staticClass: "form-control",
    attrs: {
      "type": "text",
      "name": "firstName",
      "id": "firstName",
      "required": ""
    },
    domProps: {
      "value": (_vm.firstName)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.firstName = $event.target.value
      }
    }
  })])]), _vm._v(" "), _c('div', {
    staticClass: "col-md-6"
  }, [_c('div', {
    staticClass: "form-group label-floating"
  }, [_c('label', {
    staticClass: "control-label"
  }, [_vm._v("Last Name")]), _vm._v(" "), _c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.lastName),
      expression: "lastName"
    }],
    staticClass: "form-control",
    attrs: {
      "type": "text",
      "name": "lastName",
      "id": "lastName",
      "required": ""
    },
    domProps: {
      "value": (_vm.lastName)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.lastName = $event.target.value
      }
    }
  })])])]), _vm._v(" "), _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-12"
  }, [_c('div', {
    staticClass: "form-group label-floating"
  }, [_c('label', {
    staticClass: "control-label"
  }, [_vm._v("Mobile")]), _vm._v(" "), _c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.mobile),
      expression: "mobile"
    }],
    staticClass: "form-control",
    attrs: {
      "type": "text",
      "name": "mobile",
      "id": "mobile",
      "required": ""
    },
    domProps: {
      "value": (_vm.mobile)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.mobile = $event.target.value
      }
    }
  })])])]), _vm._v(" "), _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-6"
  }, [_c('div', {
    staticClass: "form-group label-floating"
  }, [_c('label', {
    staticClass: "control-label"
  }, [_vm._v("Select Role")]), _vm._v(" "), _c('select', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.userrole),
      expression: "userrole"
    }],
    staticClass: "form-control",
    attrs: {
      "name": "userrole",
      "id": "userrole",
      "required": ""
    },
    on: {
      "change": function($event) {
        var $$selectedVal = Array.prototype.filter.call($event.target.options, function(o) {
          return o.selected
        }).map(function(o) {
          var val = "_value" in o ? o._value : o.value;
          return val
        });
        _vm.userrole = $event.target.multiple ? $$selectedVal : $$selectedVal[0]
      }
    }
  }, [_c('option', {
    attrs: {
      "value": "read"
    }
  }, [_vm._v("Read")]), _vm._v(" "), _c('option', {
    attrs: {
      "value": "write"
    }
  }, [_vm._v("Write")])])])]), _vm._v(" "), _c('div', {
    staticClass: "col-md-6"
  }, [_c('div', {
    staticClass: "form-group label-floating"
  }, [_c('label', {
    staticClass: "control-label"
  }, [_vm._v("Select Status")]), _vm._v(" "), _c('select', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.status),
      expression: "status"
    }],
    staticClass: "form-control",
    attrs: {
      "name": "status",
      "id": "status",
      "required": ""
    },
    on: {
      "change": function($event) {
        var $$selectedVal = Array.prototype.filter.call($event.target.options, function(o) {
          return o.selected
        }).map(function(o) {
          var val = "_value" in o ? o._value : o.value;
          return val
        });
        _vm.status = $event.target.multiple ? $$selectedVal : $$selectedVal[0]
      }
    }
  }, [_c('option', {
    attrs: {
      "value": "active"
    }
  }, [_vm._v("Active")]), _vm._v(" "), _c('option', {
    attrs: {
      "value": "inactive"
    }
  }, [_vm._v("InActive")])])])])]), _vm._v(" "), _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-6"
  }, [_c('div', {
    staticClass: "form-group label-floating"
  }, [_c('label', {
    staticClass: "control-label"
  }, [_vm._v("Password")]), _vm._v(" "), _c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.password),
      expression: "password"
    }],
    staticClass: "form-control",
    attrs: {
      "type": "password",
      "name": "password",
      "id": "password",
      "required": ""
    },
    domProps: {
      "value": (_vm.password)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.password = $event.target.value
      }
    }
  })])]), _vm._v(" "), _vm._m(2)]), _vm._v(" "), _c('button', {
    staticClass: "btn btn-primary pull-right",
    attrs: {
      "type": "submit"
    },
    on: {
      "click": function($event) {
        _vm.signup()
      }
    }
  }, [_c('b', [_vm._v(" SIGN UP ")])]), _vm._v(" "), _c('div', {
    staticClass: "clearfix"
  })])])])])])])])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', [_c('h2', {
    staticStyle: {
      "line-height": "12px",
      "font-size": "20pt",
      "margin-top": "20px",
      "margin-left": "15px",
      "position": "absolute",
      "top": "0",
      "left": "0",
      "color": "purple"
    }
  }, [_c('b', [_vm._v("API PLANNER APP")])])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "card-header",
    attrs: {
      "data-background-color": "purple"
    }
  }, [_c('h4', {
    staticClass: "title"
  }, [_c('b', [_vm._v(" SignUp")])])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "col-md-6"
  }, [_c('div', {
    staticClass: "form-group label-floating"
  }, [_c('label', {
    staticClass: "control-label"
  }, [_vm._v("Confirm Password")]), _vm._v(" "), _c('input', {
    staticClass: "form-control",
    attrs: {
      "type": "password",
      "name": "confirm",
      "id": "confirm",
      "required": ""
    }
  })])])
}]}

/***/ }),
/* 65 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "hello"
  }, [_c('h1', [_vm._v(_vm._s(_vm.msg))]), _vm._v(" "), _c('h2', [_vm._v("Essential Links")]), _vm._v(" "), _vm._m(0), _vm._v(" "), _c('h2', [_vm._v("Ecosystem")]), _vm._v(" "), _vm._m(1)])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('ul', [_c('li', [_c('a', {
    attrs: {
      "href": "https://vuejs.org",
      "target": "_blank"
    }
  }, [_vm._v("Core Docs")])]), _vm._v(" "), _c('li', [_c('a', {
    attrs: {
      "href": "https://forum.vuejs.org",
      "target": "_blank"
    }
  }, [_vm._v("Forum")])]), _vm._v(" "), _c('li', [_c('a', {
    attrs: {
      "href": "https://gitter.im/vuejs/vue",
      "target": "_blank"
    }
  }, [_vm._v("Gitter Chat")])]), _vm._v(" "), _c('li', [_c('a', {
    attrs: {
      "href": "https://twitter.com/vuejs",
      "target": "_blank"
    }
  }, [_vm._v("Twitter")])]), _vm._v(" "), _c('br'), _vm._v(" "), _c('li', [_c('a', {
    attrs: {
      "href": "http://vuejs-templates.github.io/webpack/",
      "target": "_blank"
    }
  }, [_vm._v("Docs for This Template")])])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('ul', [_c('li', [_c('a', {
    attrs: {
      "href": "http://router.vuejs.org/",
      "target": "_blank"
    }
  }, [_vm._v("vue-router")])]), _vm._v(" "), _c('li', [_c('a', {
    attrs: {
      "href": "http://vuex.vuejs.org/",
      "target": "_blank"
    }
  }, [_vm._v("vuex")])]), _vm._v(" "), _c('li', [_c('a', {
    attrs: {
      "href": "http://vue-loader.vuejs.org/",
      "target": "_blank"
    }
  }, [_vm._v("vue-loader")])]), _vm._v(" "), _c('li', [_c('a', {
    attrs: {
      "href": "https://github.com/vuejs/awesome-vue",
      "target": "_blank"
    }
  }, [_vm._v("awesome-vue")])])])
}]}

/***/ }),
/* 66 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "content"
  }, [_c('div', {
    staticClass: "container-fluid"
  }, [_c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-10"
  }, [_c('div', {
    staticClass: "card"
  }, [_vm._m(0), _vm._v(" "), _c('div', {
    staticClass: "card-content"
  }, [_c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-12"
  }, [_c('div', {
    staticClass: "form-group label-floating"
  }, [_c('label', {
    staticClass: "control-label",
    attrs: {
      "for": "url"
    }
  }, [_vm._v("API Url")]), _vm._v(" "), _c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.url),
      expression: "url"
    }],
    staticClass: "form-control",
    attrs: {
      "type": "text",
      "name": "url",
      "required": ""
    },
    domProps: {
      "value": (_vm.url)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.url = $event.target.value
      }
    }
  })])])]), _vm._v(" "), _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-6"
  }, [_c('div', {
    staticClass: "form-group label-floating"
  }, [_c('label', {
    staticClass: "control-label",
    attrs: {
      "for": "apiType"
    }
  }, [_vm._v("API Type")]), _vm._v(" "), _c('select', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.apiType),
      expression: "apiType"
    }],
    staticClass: "form-control",
    attrs: {
      "name": "apiType",
      "id": "apiType",
      "required": ""
    },
    on: {
      "change": function($event) {
        var $$selectedVal = Array.prototype.filter.call($event.target.options, function(o) {
          return o.selected
        }).map(function(o) {
          var val = "_value" in o ? o._value : o.value;
          return val
        });
        _vm.apiType = $event.target.multiple ? $$selectedVal : $$selectedVal[0]
      }
    }
  }, [_c('option', {
    attrs: {
      "value": "POST"
    }
  }, [_vm._v("POST")]), _vm._v(" "), _c('option', {
    attrs: {
      "value": "GET"
    }
  }, [_vm._v("GET")]), _vm._v(" "), _c('option', {
    attrs: {
      "value": "PUT"
    }
  }, [_vm._v("PUT")]), _vm._v(" "), _c('option', {
    attrs: {
      "value": "DELETE"
    }
  }, [_vm._v("DELETE")])])])])]), _vm._v(" "), _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-2"
  }, [_c('label', {
    staticClass: "control-label",
    attrs: {
      "for": "headerParameter"
    }
  }, [_vm._v(" Add header Parameter")]), _vm._v(" "), _c('button', {
    staticClass: "glyphicon glyphicon-plus-sign",
    staticStyle: {
      "color": "purple"
    },
    on: {
      "click": function($event) {
        _vm.addheader()
      }
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "col-md-5"
  }, [_c('div', {
    staticClass: "form-group label-floating"
  }, [_c('label', {
    staticClass: "control-label",
    attrs: {
      "for": "headerParameter"
    }
  }, [_vm._v(" Key")]), _vm._v(" "), _c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.key),
      expression: "key"
    }],
    staticClass: "form-control col-md-6",
    attrs: {
      "type": "text",
      "name": "headerParameter"
    },
    domProps: {
      "value": (_vm.key)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.key = $event.target.value
      }
    }
  })])]), _vm._v(" "), _c('div', {
    staticClass: "col-md-5"
  }, [_c('div', {
    staticClass: "form-group label-floating"
  }, [_c('label', {
    staticClass: "control-label",
    attrs: {
      "for": "headerParameter"
    }
  }, [_vm._v(" Value")]), _vm._v(" "), _c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.value),
      expression: "value"
    }],
    staticClass: "form-control col-md-6",
    attrs: {
      "type": "text",
      "name": "headerParameter"
    },
    domProps: {
      "value": (_vm.value)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.value = $event.target.value
      }
    }
  })])])]), _vm._v(" "), _c('table', {
    staticClass: "table table-bordered"
  }, [_vm._m(1), _vm._v(" "), _c('tbody', _vm._l((_vm.headerParameter), function(header) {
    return _c('tr', [_c('td', [_vm._v("\n                              " + _vm._s(header.key) + "\n                           ")]), _vm._v(" "), _c('td', [_vm._v("\n                              " + _vm._s(header.value) + "\n                           ")])])
  }))]), _vm._v(" "), _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-2"
  }, [_c('label', {
    staticClass: "control-label",
    attrs: {
      "for": "headerParameter"
    }
  }, [_vm._v(" Add  Parameter")]), _vm._v(" "), _c('button', {
    staticClass: "glyphicon glyphicon-plus-sign",
    staticStyle: {
      "color": "purple"
    },
    on: {
      "click": function($event) {
        _vm.addparams()
      }
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "col-md-3"
  }, [_c('div', {
    staticClass: "form-group label-floating"
  }, [_c('label', {
    staticClass: "control-label",
    attrs: {
      "for": "params"
    }
  }, [_vm._v("Params field")]), _vm._v(" "), _c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.field),
      expression: "field"
    }],
    staticClass: "form-control col-md-6",
    attrs: {
      "type": "text",
      "name": "headerParameter"
    },
    domProps: {
      "value": (_vm.field)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.field = $event.target.value
      }
    }
  })])]), _vm._v(" "), _c('div', {
    staticClass: "col-md-3"
  }, [_c('div', {
    staticClass: "form-group label-floating"
  }, [_c('label', {
    staticClass: "control-label",
    attrs: {
      "for": "params"
    }
  }, [_vm._v("Params Type")]), _vm._v(" "), _c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.type),
      expression: "type"
    }],
    staticClass: "form-control col-md-6",
    attrs: {
      "type": "text",
      "name": "headerParameter"
    },
    domProps: {
      "value": (_vm.type)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.type = $event.target.value
      }
    }
  })])]), _vm._v(" "), _c('div', {
    staticClass: "col-md-4"
  }, [_c('div', {
    staticClass: "form-group label-floating"
  }, [_c('label', {
    staticClass: "control-label",
    attrs: {
      "for": "params"
    }
  }, [_vm._v("Params Description")]), _vm._v(" "), _c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.description),
      expression: "description"
    }],
    staticClass: "form-control col-md-6",
    attrs: {
      "type": "text",
      "name": "headerParameter"
    },
    domProps: {
      "value": (_vm.description)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.description = $event.target.value
      }
    }
  })])])]), _vm._v(" "), _c('table', {
    staticClass: "table table-bordered"
  }, [_vm._m(2), _vm._v(" "), _c('tbody', _vm._l((_vm.params), function(para) {
    return _c('tr', [_c('td', [_vm._v("\n                              " + _vm._s(para.field) + "\n                           ")]), _vm._v(" "), _c('td', [_vm._v("\n                              " + _vm._s(para.type) + "\n                           ")]), _vm._v(" "), _c('td', [_vm._v("\n                              " + _vm._s(para.description) + "\n                           ")])])
  }))]), _vm._v(" "), _vm._m(3), _vm._v(" "), _c('div', {
    staticClass: "container-fluid",
    attrs: {
      "id": "exTab3"
    }
  }, [_vm._m(4), _vm._v(" "), _c('div', {
    staticClass: "tab-content clearfix"
  }, [_c('div', {
    staticClass: "tab-pane active",
    attrs: {
      "id": "1b"
    }
  }, [_c('div', [_c('div', {
    staticClass: "form-group label-floating"
  }, [_c('label', {
    staticClass: "control-label",
    attrs: {
      "for": "exampleUsage"
    }
  }, [_vm._v("example Usage")]), _vm._v(" "), _c('textarea', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.exampleUsage),
      expression: "exampleUsage"
    }],
    staticClass: "form-control",
    attrs: {
      "rows": "9",
      "name": "exampleUsage",
      "required": ""
    },
    domProps: {
      "value": (_vm.exampleUsage)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.exampleUsage = $event.target.value
      }
    }
  })])])]), _vm._v(" "), _c('div', {
    staticClass: "tab-pane",
    attrs: {
      "id": "2b"
    }
  }, [_c('div', [_c('div', {
    staticClass: "form-group label-floating"
  }, [_c('label', {
    staticClass: "control-label",
    attrs: {
      "for": "successResponse"
    }
  }, [_vm._v("Success Response")]), _vm._v(" "), _c('textarea', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.successResponse),
      expression: "successResponse"
    }],
    staticClass: "form-control",
    attrs: {
      "rows": "9",
      "name": "successResponse",
      "required": ""
    },
    domProps: {
      "value": (_vm.successResponse)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.successResponse = $event.target.value
      }
    }
  })])])]), _vm._v(" "), _c('div', {
    staticClass: "tab-pane",
    attrs: {
      "id": "3b"
    }
  }, [_c('div', [_c('div', {
    staticClass: "form-group label-floating"
  }, [_c('label', {
    staticClass: "control-label",
    attrs: {
      "for": "errorResponse"
    }
  }, [_vm._v("Error Response")]), _vm._v(" "), _c('textarea', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.errorResponse),
      expression: "errorResponse"
    }],
    staticClass: "form-control",
    attrs: {
      "rows": "9",
      "name": "errorResponse",
      "required": ""
    },
    domProps: {
      "value": (_vm.errorResponse)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.errorResponse = $event.target.value
      }
    }
  })])])])])]), _vm._v(" "), _c('div', {
    staticClass: "row",
    staticStyle: {
      "margin-top": "30px"
    }
  }, [_c('div', {
    staticStyle: {
      "margin-left": "37%"
    }
  }, [_c('button', {
    staticClass: "btn btn-primary btn-round",
    attrs: {
      "type": "reset"
    },
    on: {
      "click": function($event) {
        _vm.cancel()
      }
    }
  }, [_vm._v("Cancel")]), _vm._v(" "), _c('button', {
    staticClass: "btn btn-primary btn-round",
    attrs: {
      "type": "button"
    },
    on: {
      "click": function($event) {
        _vm.add()
      }
    }
  }, [_vm._v("Submit")])])]), _vm._v(" "), _c('div', {
    staticClass: "clearfix"
  })])])])])])])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "card-header",
    attrs: {
      "data-background-color": "purple"
    }
  }, [_c('h4', {
    staticClass: "title"
  }, [_vm._v(" Add API")]), _vm._v(" "), _c('p', {
    staticClass: "category"
  }, [_vm._v("Add API Information")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('thead', {
    staticStyle: {
      "background-color": "grey",
      "color": "white"
    }
  }, [_c('tr', [_c('th', [_c('b', [_vm._v("Key")])]), _vm._v(" "), _c('th', [_c('b', [_vm._v("value ")])])])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('thead', {
    staticStyle: {
      "background-color": "grey",
      "color": "white"
    }
  }, [_c('tr', [_c('th', [_c('b', [_vm._v("Field")])]), _vm._v(" "), _c('th', [_c('b', [_vm._v("Type ")])]), _vm._v(" "), _c('th', [_c('b', [_vm._v("Description ")])])])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "container",
    staticStyle: {
      "margin-bottom": "30px"
    }
  }, [_c('h2')])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('ul', {
    staticClass: "nav nav-pills"
  }, [_c('li', {
    staticClass: "active"
  }, [_c('a', {
    attrs: {
      "href": "#1b",
      "data-toggle": "tab"
    }
  }, [_vm._v("example Usage")])]), _vm._v(" "), _c('li', [_c('a', {
    attrs: {
      "href": "#2b",
      "data-toggle": "tab"
    }
  }, [_vm._v("Success Response")])]), _vm._v(" "), _c('li', [_c('a', {
    attrs: {
      "href": "#3b",
      "data-toggle": "tab"
    }
  }, [_vm._v("Error Response")])])])
}]}

/***/ }),
/* 67 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "container"
  }, [_c('div', {
    staticClass: "container-fluid",
    attrs: {
      "id": "tb"
    }
  }, [_c('input', {
    staticClass: "btn btn-md btn-success",
    attrs: {
      "type": "button",
      "value": "Add New Api"
    },
    on: {
      "click": function($event) {
        _vm.addAPI()
      }
    }
  }), _vm._v(" "), _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-10"
  }, [_c('div', {
    staticClass: "card"
  }, [_vm._m(0), _vm._v(" "), _c('div', {
    staticClass: "card-content table-responsive"
  }, [_c('table', {
    staticClass: "table table-hover"
  }, [_vm._m(1), _vm._v(" "), _c('tbody', _vm._l((_vm.APIs), function(api) {
    return _c('tr', [_c('td', [_vm._v(_vm._s(api.apiType))]), _vm._v(" "), _c('td', [_c('b', [_vm._v("/")]), _vm._v(_vm._s(api.url))]), _vm._v(" "), _c('td', [_c('input', {
      staticClass: "btn btn-sm  btn-primary",
      attrs: {
        "type": "button",
        "value": "Documentation"
      },
      on: {
        "click": function($event) {
          _vm.documentation(api)
        }
      }
    }), _vm._v(" "), _c('input', {
      staticClass: "btn btn-sm  btn-primary",
      attrs: {
        "type": "button",
        "value": "Edit"
      },
      on: {
        "click": function($event) {
          _vm.div_show1(api)
        }
      }
    }), _vm._v(" "), _c('input', {
      staticClass: "btn btn-sm  btn-danger",
      attrs: {
        "type": "button",
        "value": "delete"
      },
      on: {
        "click": function($event) {
          _vm.deleteapi(api)
        }
      }
    })])])
  }))])])])])])]), _vm._v(" "), _c('div', {
    staticStyle: {
      "display": "none"
    },
    attrs: {
      "id": "frm1"
    }
  }, [_c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-8"
  }, [_c('div', {
    staticClass: "card"
  }, [_vm._m(2), _vm._v(" "), _c('div', {
    staticClass: "card-content"
  }, [_c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-12"
  }, [_c('div', {
    staticClass: "form-group label-floating"
  }, [_c('label', {
    staticClass: "control-label",
    attrs: {
      "for": "url"
    }
  }, [_vm._v("API Url")]), _vm._v(" "), _c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.url),
      expression: "url"
    }],
    staticClass: "form-control",
    attrs: {
      "type": "text",
      "name": "url",
      "required": ""
    },
    domProps: {
      "value": (_vm.url)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.url = $event.target.value
      }
    }
  })])])]), _vm._v(" "), _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-6"
  }, [_c('div', {
    staticClass: "form-group label-floating"
  }, [_c('label', {
    staticClass: "control-label",
    attrs: {
      "for": "apiType"
    }
  }, [_vm._v("API Type")]), _vm._v(" "), _c('select', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.apiType),
      expression: "apiType"
    }],
    staticClass: "form-control",
    attrs: {
      "name": "apiType",
      "id": "apiType",
      "required": ""
    },
    on: {
      "change": function($event) {
        var $$selectedVal = Array.prototype.filter.call($event.target.options, function(o) {
          return o.selected
        }).map(function(o) {
          var val = "_value" in o ? o._value : o.value;
          return val
        });
        _vm.apiType = $event.target.multiple ? $$selectedVal : $$selectedVal[0]
      }
    }
  }, [_c('option', {
    attrs: {
      "value": "POST"
    }
  }, [_vm._v("POST")]), _vm._v(" "), _c('option', {
    attrs: {
      "value": "GET"
    }
  }, [_vm._v("GET")]), _vm._v(" "), _c('option', {
    attrs: {
      "value": "PUT"
    }
  }, [_vm._v("PUT")]), _vm._v(" "), _c('option', {
    attrs: {
      "value": "DELETE"
    }
  }, [_vm._v("DELETE")])])])])]), _vm._v(" "), _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-2"
  }, [_c('label', {
    staticClass: "control-label",
    attrs: {
      "for": "headerParameter"
    }
  }, [_vm._v(" Add header Parameter")]), _vm._v(" "), _c('button', {
    staticClass: "glyphicon glyphicon-plus-sign",
    staticStyle: {
      "color": "purple"
    },
    on: {
      "click": function($event) {
        _vm.addheader()
      }
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "col-md-5"
  }, [_c('div', {
    staticClass: "form-group label-floating"
  }, [_c('label', {
    staticClass: "control-label",
    attrs: {
      "for": "headerParameter"
    }
  }, [_vm._v(" Key")]), _vm._v(" "), _c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.key),
      expression: "key"
    }],
    staticClass: "form-control col-md-6",
    attrs: {
      "type": "text",
      "name": "headerParameter"
    },
    domProps: {
      "value": (_vm.key)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.key = $event.target.value
      }
    }
  })])]), _vm._v(" "), _c('div', {
    staticClass: "col-md-5"
  }, [_c('div', {
    staticClass: "form-group label-floating"
  }, [_c('label', {
    staticClass: "control-label",
    attrs: {
      "for": "headerParameter"
    }
  }, [_vm._v(" Value")]), _vm._v(" "), _c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.value),
      expression: "value"
    }],
    staticClass: "form-control col-md-6",
    attrs: {
      "type": "text",
      "name": "headerParameter"
    },
    domProps: {
      "value": (_vm.value)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.value = $event.target.value
      }
    }
  })])])]), _vm._v(" "), _c('table', {
    staticClass: "table table-bordered"
  }, [_vm._m(3), _vm._v(" "), _c('tbody', _vm._l((_vm.headerParameter), function(header) {
    return _c('tr', [_c('td', [_c('input', {
      directives: [{
        name: "model",
        rawName: "v-model",
        value: (header.key),
        expression: "header.key"
      }],
      staticClass: "form-control col-md-6",
      attrs: {
        "type": "text",
        "name": "headerParameter"
      },
      domProps: {
        "value": (header.key)
      },
      on: {
        "input": function($event) {
          if ($event.target.composing) { return; }
          _vm.$set(header, "key", $event.target.value)
        }
      }
    })]), _vm._v(" "), _c('td', [_c('input', {
      directives: [{
        name: "model",
        rawName: "v-model",
        value: (header.value),
        expression: "header.value"
      }],
      staticClass: "form-control col-md-6",
      attrs: {
        "type": "text",
        "name": "headerParameter"
      },
      domProps: {
        "value": (header.value)
      },
      on: {
        "input": function($event) {
          if ($event.target.composing) { return; }
          _vm.$set(header, "value", $event.target.value)
        }
      }
    })])])
  }))]), _vm._v(" "), _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-2"
  }, [_c('label', {
    staticClass: "control-label",
    attrs: {
      "for": "headerParameter"
    }
  }, [_vm._v(" Add  Parameter")]), _vm._v(" "), _c('button', {
    staticClass: "glyphicon glyphicon-plus-sign",
    staticStyle: {
      "color": "purple"
    },
    on: {
      "click": function($event) {
        _vm.addparams()
      }
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "col-md-3"
  }, [_c('div', {
    staticClass: "form-group label-floating"
  }, [_c('label', {
    staticClass: "control-label",
    attrs: {
      "for": "params"
    }
  }, [_vm._v("Params field")]), _vm._v(" "), _c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.field),
      expression: "field"
    }],
    staticClass: "form-control col-md-6",
    attrs: {
      "type": "text",
      "name": "headerParameter"
    },
    domProps: {
      "value": (_vm.field)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.field = $event.target.value
      }
    }
  })])]), _vm._v(" "), _c('div', {
    staticClass: "col-md-3"
  }, [_c('div', {
    staticClass: "form-group label-floating"
  }, [_c('label', {
    staticClass: "control-label",
    attrs: {
      "for": "params"
    }
  }, [_vm._v("Params Type")]), _vm._v(" "), _c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.type),
      expression: "type"
    }],
    staticClass: "form-control col-md-6",
    attrs: {
      "type": "text",
      "name": "headerParameter"
    },
    domProps: {
      "value": (_vm.type)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.type = $event.target.value
      }
    }
  })])]), _vm._v(" "), _c('div', {
    staticClass: "col-md-4"
  }, [_c('div', {
    staticClass: "form-group label-floating"
  }, [_c('label', {
    staticClass: "control-label",
    attrs: {
      "for": "params"
    }
  }, [_vm._v("Params Description")]), _vm._v(" "), _c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.description),
      expression: "description"
    }],
    staticClass: "form-control col-md-6",
    attrs: {
      "type": "text",
      "name": "headerParameter"
    },
    domProps: {
      "value": (_vm.description)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.description = $event.target.value
      }
    }
  })])])]), _vm._v(" "), _c('table', {
    staticClass: "table table-bordered"
  }, [_vm._m(4), _vm._v(" "), _c('tbody', _vm._l((_vm.params), function(para) {
    return _c('tr', [_c('td', [_c('input', {
      directives: [{
        name: "model",
        rawName: "v-model",
        value: (para.field),
        expression: "para.field"
      }],
      staticClass: "form-control col-md-6",
      attrs: {
        "type": "text",
        "name": "headerParameter"
      },
      domProps: {
        "value": (para.field)
      },
      on: {
        "input": function($event) {
          if ($event.target.composing) { return; }
          _vm.$set(para, "field", $event.target.value)
        }
      }
    })]), _vm._v(" "), _c('td', [_c('input', {
      directives: [{
        name: "model",
        rawName: "v-model",
        value: (para.type),
        expression: "para.type"
      }],
      staticClass: "form-control col-md-6",
      attrs: {
        "type": "text",
        "name": "headerParameter"
      },
      domProps: {
        "value": (para.type)
      },
      on: {
        "input": function($event) {
          if ($event.target.composing) { return; }
          _vm.$set(para, "type", $event.target.value)
        }
      }
    })]), _vm._v(" "), _c('td', [_c('input', {
      directives: [{
        name: "model",
        rawName: "v-model",
        value: (para.description),
        expression: "para.description"
      }],
      staticClass: "form-control col-md-6",
      attrs: {
        "type": "text",
        "name": "headerParameter"
      },
      domProps: {
        "value": (para.description)
      },
      on: {
        "input": function($event) {
          if ($event.target.composing) { return; }
          _vm.$set(para, "description", $event.target.value)
        }
      }
    })])])
  }))]), _vm._v(" "), _vm._m(5), _vm._v(" "), _c('div', {
    staticClass: "container-fluid",
    attrs: {
      "id": "exTab3"
    }
  }, [_vm._m(6), _vm._v(" "), _c('div', {
    staticClass: "tab-content clearfix"
  }, [_c('div', {
    staticClass: "tab-pane active",
    attrs: {
      "id": "1b"
    }
  }, [_c('div', [_c('div', {
    staticClass: "form-group label-floating"
  }, [_c('label', {
    staticClass: "control-label",
    attrs: {
      "for": "exampleUsage"
    }
  }, [_vm._v("example Usage")]), _vm._v(" "), _c('textarea', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.exampleUsage),
      expression: "exampleUsage"
    }],
    staticClass: "form-control",
    attrs: {
      "rows": "9",
      "name": "exampleUsage",
      "required": ""
    },
    domProps: {
      "value": (_vm.exampleUsage)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.exampleUsage = $event.target.value
      }
    }
  })])])]), _vm._v(" "), _c('div', {
    staticClass: "tab-pane",
    attrs: {
      "id": "2b"
    }
  }, [_c('div', [_c('div', {
    staticClass: "form-group label-floating"
  }, [_c('label', {
    staticClass: "control-label",
    attrs: {
      "for": "successResponse"
    }
  }, [_vm._v("Success Response")]), _vm._v(" "), _c('textarea', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.successResponse),
      expression: "successResponse"
    }],
    staticClass: "form-control",
    attrs: {
      "rows": "9",
      "name": "successResponse",
      "required": ""
    },
    domProps: {
      "value": (_vm.successResponse)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.successResponse = $event.target.value
      }
    }
  })])])]), _vm._v(" "), _c('div', {
    staticClass: "tab-pane",
    attrs: {
      "id": "3b"
    }
  }, [_c('div', [_c('div', {
    staticClass: "form-group label-floating"
  }, [_c('label', {
    staticClass: "control-label",
    attrs: {
      "for": "errorResponse"
    }
  }, [_vm._v("Error Response")]), _vm._v(" "), _c('textarea', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.errorResponse),
      expression: "errorResponse"
    }],
    staticClass: "form-control",
    attrs: {
      "rows": "9",
      "name": "errorResponse",
      "required": ""
    },
    domProps: {
      "value": (_vm.errorResponse)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.errorResponse = $event.target.value
      }
    }
  })])])])])]), _vm._v(" "), _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticStyle: {
      "margin-left": "30%"
    }
  }, [_c('button', {
    staticClass: "btn btn-primary btn-round",
    attrs: {
      "type": "reset"
    },
    on: {
      "click": function($event) {
        _vm.cancelop()
      }
    }
  }, [_vm._v("Cancel")]), _vm._v(" "), _c('button', {
    staticClass: "btn btn-primary btn-round",
    attrs: {
      "type": "button"
    },
    on: {
      "click": function($event) {
        _vm.div_hide1()
      }
    }
  }, [_vm._v("Submit")])])]), _vm._v(" "), _c('div', {
    staticClass: "clearfix"
  })])])])])])])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "card-header",
    attrs: {
      "data-background-color": "purple"
    }
  }, [_c('h4', {
    staticClass: "title"
  }, [_vm._v("API List")]), _vm._v(" "), _c('p', {
    staticClass: "category"
  }, [_vm._v("Following are the apis of selected Servise")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('thead', [_c('th', [_vm._v("Name")]), _vm._v(" "), _c('th', [_vm._v("URL")]), _vm._v(" "), _c('th', [_vm._v("Actions")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "card-header",
    attrs: {
      "data-background-color": "purple"
    }
  }, [_c('h4', {
    staticClass: "title"
  }, [_vm._v(" Edit API")]), _vm._v(" "), _c('p', {
    staticClass: "category"
  }, [_vm._v("Edit API Information")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('thead', {
    staticStyle: {
      "background-color": "grey",
      "color": "white"
    }
  }, [_c('tr', [_c('th', [_c('b', [_vm._v("Key")])]), _vm._v(" "), _c('th', [_c('b', [_vm._v("value ")])])])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('thead', {
    staticStyle: {
      "background-color": "grey",
      "color": "white"
    }
  }, [_c('tr', [_c('th', [_c('b', [_vm._v("Field")])]), _vm._v(" "), _c('th', [_c('b', [_vm._v("Type ")])]), _vm._v(" "), _c('th', [_c('b', [_vm._v("Description ")])])])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "container",
    staticStyle: {
      "margin-bottom": "30px"
    }
  }, [_c('h2')])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('ul', {
    staticClass: "nav nav-pills"
  }, [_c('li', {
    staticClass: "active"
  }, [_c('a', {
    attrs: {
      "href": "#1b",
      "data-toggle": "tab"
    }
  }, [_vm._v("example Usage")])]), _vm._v(" "), _c('li', [_c('a', {
    attrs: {
      "href": "#2b",
      "data-toggle": "tab"
    }
  }, [_vm._v("Success Response")])]), _vm._v(" "), _c('li', [_c('a', {
    attrs: {
      "href": "#3b",
      "data-toggle": "tab"
    }
  }, [_vm._v("Error Response")])])])
}]}

/***/ }),
/* 68 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "content"
  }, [_c('div', {
    staticClass: "container-fluid"
  }, [_c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-lg-3 col-md-6 col-sm-6"
  }, [_c('div', {
    staticClass: "card card-stats"
  }, [_vm._m(0), _vm._v(" "), _c('div', {
    staticClass: "card-content"
  }, [_c('p', {
    staticClass: "category"
  }, [_vm._v("Projects")]), _vm._v(" "), _c('h3', {
    staticClass: "title"
  }, [_vm._v(_vm._s(_vm.projectCOunt)), _c('small')])]), _vm._v(" "), _vm._m(1)])]), _vm._v(" "), _vm._m(2), _vm._v(" "), _c('div', {
    staticClass: "col-lg-3 col-md-6 col-sm-6"
  }, [_c('div', {
    staticClass: "card card-stats"
  }, [_vm._m(3), _vm._v(" "), _c('div', {
    staticClass: "card-content"
  }, [_c('p', {
    staticClass: "category"
  }, [_vm._v("All API's")]), _vm._v(" "), _c('h3', {
    staticClass: "title"
  }, [_vm._v(_vm._s(_vm.apicount))])]), _vm._v(" "), _vm._m(4)])])]), _vm._v(" "), _vm._m(5), _vm._v(" "), _vm._m(6)])])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "card-header",
    attrs: {
      "data-background-color": "orange"
    }
  }, [_c('i', {
    staticClass: "material-icons"
  }, [_vm._v("content_copy")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "card-footer"
  }, [_c('div', {
    staticClass: "stats"
  }, [_c('i', {
    staticClass: "material-icons text-danger"
  }, [_vm._v("warning")]), _vm._v(" "), _c('a', {
    attrs: {
      "href": "#pablo"
    }
  }, [_vm._v("Get More Space...")])])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "col-lg-3 col-md-6 col-sm-6"
  }, [_c('div', {
    staticClass: "card card-stats"
  }, [_c('div', {
    staticClass: "card-header",
    attrs: {
      "data-background-color": "green"
    }
  }, [_c('i', {
    staticClass: "material-icons"
  }, [_vm._v("store")])]), _vm._v(" "), _c('div', {
    staticClass: "card-content"
  }, [_c('p', {
    staticClass: "category"
  }, [_vm._v("Services")]), _vm._v(" "), _c('h3', {
    staticClass: "title"
  }, [_vm._v("8")])]), _vm._v(" "), _c('div', {
    staticClass: "card-footer"
  }, [_c('div', {
    staticClass: "stats"
  }, [_c('i', {
    staticClass: "material-icons"
  }, [_vm._v("date_range")]), _vm._v(" Last 24 Hours\n\t\t\t\t\t\t\t\t\t")])])])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "card-header",
    attrs: {
      "data-background-color": "red"
    }
  }, [_c('i', {
    staticClass: "material-icons"
  }, [_vm._v("info_outline")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "card-footer"
  }, [_c('div', {
    staticClass: "stats"
  }, [_c('i', {
    staticClass: "material-icons"
  }, [_vm._v("local_offer")]), _vm._v(" Tracked from Github\n\t\t\t\t\t\t\t\t\t")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-4"
  }, [_c('div', {
    staticClass: "card"
  }, [_c('div', {
    staticClass: "card-header card-chart",
    attrs: {
      "data-background-color": "green"
    }
  }, [_c('div', {
    staticClass: "ct-chart",
    attrs: {
      "id": "dailySalesChart"
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "card-content"
  }, [_c('h4', {
    staticClass: "title"
  }, [_vm._v("Daily Sales")]), _vm._v(" "), _c('p', {
    staticClass: "category"
  }, [_c('span', {
    staticClass: "text-success"
  }, [_c('i', {
    staticClass: "fa fa-long-arrow-up"
  }), _vm._v(" 55%  ")]), _vm._v(" increase in today sales.")])]), _vm._v(" "), _c('div', {
    staticClass: "card-footer"
  }, [_c('div', {
    staticClass: "stats"
  }, [_c('i', {
    staticClass: "material-icons"
  }, [_vm._v("access_time")]), _vm._v(" updated 4 minutes ago\n\t\t\t\t\t\t\t\t\t")])])])]), _vm._v(" "), _c('div', {
    staticClass: "col-md-4"
  }, [_c('div', {
    staticClass: "card"
  }, [_c('div', {
    staticClass: "card-header card-chart",
    attrs: {
      "data-background-color": "orange"
    }
  }, [_c('div', {
    staticClass: "ct-chart",
    attrs: {
      "id": "emailsSubscriptionChart"
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "card-content"
  }, [_c('h4', {
    staticClass: "title"
  }, [_vm._v("Email Subscriptions")]), _vm._v(" "), _c('p', {
    staticClass: "category"
  }, [_vm._v("Last Campaign Performance")])]), _vm._v(" "), _c('div', {
    staticClass: "card-footer"
  }, [_c('div', {
    staticClass: "stats"
  }, [_c('i', {
    staticClass: "material-icons"
  }, [_vm._v("access_time")]), _vm._v(" campaign sent 2 days ago\n\t\t\t\t\t\t\t\t\t")])])])]), _vm._v(" "), _c('div', {
    staticClass: "col-md-4"
  }, [_c('div', {
    staticClass: "card"
  }, [_c('div', {
    staticClass: "card-header card-chart",
    attrs: {
      "data-background-color": "red"
    }
  }, [_c('div', {
    staticClass: "ct-chart",
    attrs: {
      "id": "completedTasksChart"
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "card-content"
  }, [_c('h4', {
    staticClass: "title"
  }, [_vm._v("Completed Tasks")]), _vm._v(" "), _c('p', {
    staticClass: "category"
  }, [_vm._v("Last Campaign Performance")])]), _vm._v(" "), _c('div', {
    staticClass: "card-footer"
  }, [_c('div', {
    staticClass: "stats"
  }, [_c('i', {
    staticClass: "material-icons"
  }, [_vm._v("access_time")]), _vm._v(" campaign sent 2 days ago\n\t\t\t\t\t\t\t\t\t")])])])])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-lg-6 col-md-12"
  }, [_c('div', {
    staticClass: "card card-nav-tabs"
  }, [_c('div', {
    staticClass: "card-header",
    attrs: {
      "data-background-color": "purple"
    }
  }, [_c('div', {
    staticClass: "nav-tabs-navigation"
  }, [_c('div', {
    staticClass: "nav-tabs-wrapper"
  }, [_c('span', {
    staticClass: "nav-tabs-title"
  }, [_vm._v("Tasks:")]), _vm._v(" "), _c('ul', {
    staticClass: "nav nav-tabs",
    attrs: {
      "data-tabs": "tabs"
    }
  }, [_c('li', {
    staticClass: "active"
  }, [_c('a', {
    attrs: {
      "href": "#profile",
      "data-toggle": "tab"
    }
  }, [_c('i', {
    staticClass: "material-icons"
  }, [_vm._v("bug_report")]), _vm._v("\n\t\t\t\t\t\t\t\t\t\t\t\t\t\tBugs\n\t\t\t\t\t\t\t\t\t\t\t\t\t"), _c('div', {
    staticClass: "ripple-container"
  })])]), _vm._v(" "), _c('li', {}, [_c('a', {
    attrs: {
      "href": "#messages",
      "data-toggle": "tab"
    }
  }, [_c('i', {
    staticClass: "material-icons"
  }, [_vm._v("code")]), _vm._v("\n\t\t\t\t\t\t\t\t\t\t\t\t\t\tWebsite\n\t\t\t\t\t\t\t\t\t\t\t\t\t"), _c('div', {
    staticClass: "ripple-container"
  })])]), _vm._v(" "), _c('li', {}, [_c('a', {
    attrs: {
      "href": "#settings",
      "data-toggle": "tab"
    }
  }, [_c('i', {
    staticClass: "material-icons"
  }, [_vm._v("cloud")]), _vm._v("\n\t\t\t\t\t\t\t\t\t\t\t\t\t\tServer\n\t\t\t\t\t\t\t\t\t\t\t\t\t"), _c('div', {
    staticClass: "ripple-container"
  })])])])])])]), _vm._v(" "), _c('div', {
    staticClass: "card-content"
  }, [_c('div', {
    staticClass: "tab-content"
  }, [_c('div', {
    staticClass: "tab-pane active",
    attrs: {
      "id": "profile"
    }
  }, [_c('table', {
    staticClass: "table"
  }, [_c('tbody', [_c('tr', [_c('td', [_c('div', {
    staticClass: "checkbox"
  }, [_c('label', [_c('input', {
    attrs: {
      "type": "checkbox",
      "name": "optionsCheckboxes",
      "checked": ""
    }
  })])])]), _vm._v(" "), _c('td', [_vm._v("Sign contract for \"What are conference organizers afraid of?\"")]), _vm._v(" "), _c('td', {
    staticClass: "td-actions text-right"
  }, [_c('button', {
    staticClass: "btn btn-primary btn-simple btn-xs",
    attrs: {
      "type": "button",
      "rel": "tooltip",
      "title": "Edit Task"
    }
  }, [_c('i', {
    staticClass: "material-icons"
  }, [_vm._v("edit")])]), _vm._v(" "), _c('button', {
    staticClass: "btn btn-danger btn-simple btn-xs",
    attrs: {
      "type": "button",
      "rel": "tooltip",
      "title": "Remove"
    }
  }, [_c('i', {
    staticClass: "material-icons"
  }, [_vm._v("close")])])])]), _vm._v(" "), _c('tr', [_c('td', [_c('div', {
    staticClass: "checkbox"
  }, [_c('label', [_c('input', {
    attrs: {
      "type": "checkbox",
      "name": "optionsCheckboxes"
    }
  })])])]), _vm._v(" "), _c('td', [_vm._v("Lines From Great Russian Literature? Or E-mails From My Boss?")]), _vm._v(" "), _c('td', {
    staticClass: "td-actions text-right"
  }, [_c('button', {
    staticClass: "btn btn-primary btn-simple btn-xs",
    attrs: {
      "type": "button",
      "rel": "tooltip",
      "title": "Edit Task"
    }
  }, [_c('i', {
    staticClass: "material-icons"
  }, [_vm._v("edit")])]), _vm._v(" "), _c('button', {
    staticClass: "btn btn-danger btn-simple btn-xs",
    attrs: {
      "type": "button",
      "rel": "tooltip",
      "title": "Remove"
    }
  }, [_c('i', {
    staticClass: "material-icons"
  }, [_vm._v("close")])])])]), _vm._v(" "), _c('tr', [_c('td', [_c('div', {
    staticClass: "checkbox"
  }, [_c('label', [_c('input', {
    attrs: {
      "type": "checkbox",
      "name": "optionsCheckboxes"
    }
  })])])]), _vm._v(" "), _c('td', [_vm._v("Flooded: One year later, assessing what was lost and what was found when a ravaging rain swept through metro Detroit\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t")]), _vm._v(" "), _c('td', {
    staticClass: "td-actions text-right"
  }, [_c('button', {
    staticClass: "btn btn-primary btn-simple btn-xs",
    attrs: {
      "type": "button",
      "rel": "tooltip",
      "title": "Edit Task"
    }
  }, [_c('i', {
    staticClass: "material-icons"
  }, [_vm._v("edit")])]), _vm._v(" "), _c('button', {
    staticClass: "btn btn-danger btn-simple btn-xs",
    attrs: {
      "type": "button",
      "rel": "tooltip",
      "title": "Remove"
    }
  }, [_c('i', {
    staticClass: "material-icons"
  }, [_vm._v("close")])])])]), _vm._v(" "), _c('tr', [_c('td', [_c('div', {
    staticClass: "checkbox"
  }, [_c('label', [_c('input', {
    attrs: {
      "type": "checkbox",
      "name": "optionsCheckboxes",
      "checked": ""
    }
  })])])]), _vm._v(" "), _c('td', [_vm._v("Create 4 Invisible User Experiences you Never Knew About")]), _vm._v(" "), _c('td', {
    staticClass: "td-actions text-right"
  }, [_c('button', {
    staticClass: "btn btn-primary btn-simple btn-xs",
    attrs: {
      "type": "button",
      "rel": "tooltip",
      "title": "Edit Task"
    }
  }, [_c('i', {
    staticClass: "material-icons"
  }, [_vm._v("edit")])]), _vm._v(" "), _c('button', {
    staticClass: "btn btn-danger btn-simple btn-xs",
    attrs: {
      "type": "button",
      "rel": "tooltip",
      "title": "Remove"
    }
  }, [_c('i', {
    staticClass: "material-icons"
  }, [_vm._v("close")])])])])])])]), _vm._v(" "), _c('div', {
    staticClass: "tab-pane",
    attrs: {
      "id": "messages"
    }
  }, [_c('table', {
    staticClass: "table"
  }, [_c('tbody', [_c('tr', [_c('td', [_c('div', {
    staticClass: "checkbox"
  }, [_c('label', [_c('input', {
    attrs: {
      "type": "checkbox",
      "name": "optionsCheckboxes",
      "checked": ""
    }
  })])])]), _vm._v(" "), _c('td', [_vm._v("Flooded: One year later, assessing what was lost and what was found when a ravaging rain swept through metro Detroit\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t")]), _vm._v(" "), _c('td', {
    staticClass: "td-actions text-right"
  }, [_c('button', {
    staticClass: "btn btn-primary btn-simple btn-xs",
    attrs: {
      "type": "button",
      "rel": "tooltip",
      "title": "Edit Task"
    }
  }, [_c('i', {
    staticClass: "material-icons"
  }, [_vm._v("edit")])]), _vm._v(" "), _c('button', {
    staticClass: "btn btn-danger btn-simple btn-xs",
    attrs: {
      "type": "button",
      "rel": "tooltip",
      "title": "Remove"
    }
  }, [_c('i', {
    staticClass: "material-icons"
  }, [_vm._v("close")])])])]), _vm._v(" "), _c('tr', [_c('td', [_c('div', {
    staticClass: "checkbox"
  }, [_c('label', [_c('input', {
    attrs: {
      "type": "checkbox",
      "name": "optionsCheckboxes"
    }
  })])])]), _vm._v(" "), _c('td', [_vm._v("Sign contract for \"What are conference organizers afraid of?\"")]), _vm._v(" "), _c('td', {
    staticClass: "td-actions text-right"
  }, [_c('button', {
    staticClass: "btn btn-primary btn-simple btn-xs",
    attrs: {
      "type": "button",
      "rel": "tooltip",
      "title": "Edit Task"
    }
  }, [_c('i', {
    staticClass: "material-icons"
  }, [_vm._v("edit")])]), _vm._v(" "), _c('button', {
    staticClass: "btn btn-danger btn-simple btn-xs",
    attrs: {
      "type": "button",
      "rel": "tooltip",
      "title": "Remove"
    }
  }, [_c('i', {
    staticClass: "material-icons"
  }, [_vm._v("close")])])])])])])]), _vm._v(" "), _c('div', {
    staticClass: "tab-pane",
    attrs: {
      "id": "settings"
    }
  }, [_c('table', {
    staticClass: "table"
  }, [_c('tbody', [_c('tr', [_c('td', [_c('div', {
    staticClass: "checkbox"
  }, [_c('label', [_c('input', {
    attrs: {
      "type": "checkbox",
      "name": "optionsCheckboxes"
    }
  })])])]), _vm._v(" "), _c('td', [_vm._v("Lines From Great Russian Literature? Or E-mails From My Boss?")]), _vm._v(" "), _c('td', {
    staticClass: "td-actions text-right"
  }, [_c('button', {
    staticClass: "btn btn-primary btn-simple btn-xs",
    attrs: {
      "type": "button",
      "rel": "tooltip",
      "title": "Edit Task"
    }
  }, [_c('i', {
    staticClass: "material-icons"
  }, [_vm._v("edit")])]), _vm._v(" "), _c('button', {
    staticClass: "btn btn-danger btn-simple btn-xs",
    attrs: {
      "type": "button",
      "rel": "tooltip",
      "title": "Remove"
    }
  }, [_c('i', {
    staticClass: "material-icons"
  }, [_vm._v("close")])])])]), _vm._v(" "), _c('tr', [_c('td', [_c('div', {
    staticClass: "checkbox"
  }, [_c('label', [_c('input', {
    attrs: {
      "type": "checkbox",
      "name": "optionsCheckboxes",
      "checked": ""
    }
  })])])]), _vm._v(" "), _c('td', [_vm._v("Flooded: One year later, assessing what was lost and what was found when a ravaging rain swept through metro Detroit\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t")]), _vm._v(" "), _c('td', {
    staticClass: "td-actions text-right"
  }, [_c('button', {
    staticClass: "btn btn-primary btn-simple btn-xs",
    attrs: {
      "type": "button",
      "rel": "tooltip",
      "title": "Edit Task"
    }
  }, [_c('i', {
    staticClass: "material-icons"
  }, [_vm._v("edit")])]), _vm._v(" "), _c('button', {
    staticClass: "btn btn-danger btn-simple btn-xs",
    attrs: {
      "type": "button",
      "rel": "tooltip",
      "title": "Remove"
    }
  }, [_c('i', {
    staticClass: "material-icons"
  }, [_vm._v("close")])])])]), _vm._v(" "), _c('tr', [_c('td', [_c('div', {
    staticClass: "checkbox"
  }, [_c('label', [_c('input', {
    attrs: {
      "type": "checkbox",
      "name": "optionsCheckboxes"
    }
  })])])]), _vm._v(" "), _c('td', [_vm._v("Sign contract for \"What are conference organizers afraid of?\"")]), _vm._v(" "), _c('td', {
    staticClass: "td-actions text-right"
  }, [_c('button', {
    staticClass: "btn btn-primary btn-simple btn-xs",
    attrs: {
      "type": "button",
      "rel": "tooltip",
      "title": "Edit Task"
    }
  }, [_c('i', {
    staticClass: "material-icons"
  }, [_vm._v("edit")])]), _vm._v(" "), _c('button', {
    staticClass: "btn btn-danger btn-simple btn-xs",
    attrs: {
      "type": "button",
      "rel": "tooltip",
      "title": "Remove"
    }
  }, [_c('i', {
    staticClass: "material-icons"
  }, [_vm._v("close")])])])])])])])])])])]), _vm._v(" "), _c('div', {
    staticClass: "col-lg-6 col-md-12"
  }, [_c('div', {
    staticClass: "card"
  }, [_c('div', {
    staticClass: "card-header",
    attrs: {
      "data-background-color": "orange"
    }
  }, [_c('h4', {
    staticClass: "title"
  }, [_vm._v("Employees Stats")]), _vm._v(" "), _c('p', {
    staticClass: "category"
  }, [_vm._v("New employees on 15th September, 2016")])]), _vm._v(" "), _c('div', {
    staticClass: "card-content table-responsive"
  }, [_c('table', {
    staticClass: "table table-hover"
  }, [_c('thead', {
    staticClass: "text-warning"
  }, [_c('th', [_vm._v("ID")]), _vm._v(" "), _c('th', [_vm._v("Name")]), _vm._v(" "), _c('th', [_vm._v("Salary")]), _vm._v(" "), _c('th', [_vm._v("Country")])]), _vm._v(" "), _c('tbody', [_c('tr', [_c('td', [_vm._v("1")]), _vm._v(" "), _c('td', [_vm._v("Dakota Rice")]), _vm._v(" "), _c('td', [_vm._v("$36,738")]), _vm._v(" "), _c('td', [_vm._v("Niger")])]), _vm._v(" "), _c('tr', [_c('td', [_vm._v("2")]), _vm._v(" "), _c('td', [_vm._v("Minerva Hooper")]), _vm._v(" "), _c('td', [_vm._v("$23,789")]), _vm._v(" "), _c('td', [_vm._v("Curaçao")])]), _vm._v(" "), _c('tr', [_c('td', [_vm._v("3")]), _vm._v(" "), _c('td', [_vm._v("Sage Rodriguez")]), _vm._v(" "), _c('td', [_vm._v("$56,142")]), _vm._v(" "), _c('td', [_vm._v("Netherlands")])]), _vm._v(" "), _c('tr', [_c('td', [_vm._v("4")]), _vm._v(" "), _c('td', [_vm._v("Philip Chaney")]), _vm._v(" "), _c('td', [_vm._v("$38,735")]), _vm._v(" "), _c('td', [_vm._v("Korea, South")])])])])])])])])
}]}

/***/ }),
/* 69 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-8"
  }, [_c('div', {
    staticClass: "card"
  }, [_vm._m(0), _vm._v(" "), _c('div', {
    staticClass: "card-content"
  }, [_c('form', [_c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-12"
  }, [_c('div', {
    staticClass: "form-group label-floating"
  }, [_c('label', {
    staticClass: "control-label",
    attrs: {
      "for": "name"
    }
  }, [_vm._v("name")]), _vm._v(" "), _c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.name),
      expression: "name"
    }],
    staticClass: "form-control",
    attrs: {
      "type": "text",
      "name": "name",
      "required": ""
    },
    domProps: {
      "value": (_vm.name)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.name = $event.target.value
      }
    }
  })])])]), _vm._v(" "), _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-6"
  }, [_c('div', {
    staticClass: "form-group label-floating"
  }, [_c('label', {
    staticClass: "control-label",
    attrs: {
      "for": "status"
    }
  }, [_vm._v("status")]), _vm._v(" "), _c('select', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.status),
      expression: "status"
    }],
    staticClass: "form-control",
    attrs: {
      "name": "status",
      "id": "status",
      "required": ""
    },
    on: {
      "change": function($event) {
        var $$selectedVal = Array.prototype.filter.call($event.target.options, function(o) {
          return o.selected
        }).map(function(o) {
          var val = "_value" in o ? o._value : o.value;
          return val
        });
        _vm.status = $event.target.multiple ? $$selectedVal : $$selectedVal[0]
      }
    }
  }, [_c('option', {
    attrs: {
      "value": "active"
    }
  }, [_vm._v("Active")]), _vm._v(" "), _c('option', {
    attrs: {
      "value": "inactive"
    }
  }, [_vm._v("InActive")])])])])]), _vm._v(" "), _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-12"
  }, [_c('div', {
    staticClass: "form-group label-floating"
  }, [_c('label', {
    staticClass: "control-label",
    attrs: {
      "for": "description"
    }
  }, [_vm._v(" Description")]), _vm._v(" "), _c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.description),
      expression: "description"
    }],
    staticClass: "form-control",
    attrs: {
      "type": "text",
      "name": "description",
      "required": ""
    },
    domProps: {
      "value": (_vm.description)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.description = $event.target.value
      }
    }
  })])])]), _vm._v(" "), _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticStyle: {
      "margin-left": "30%"
    }
  }, [_c('button', {
    staticClass: "btn btn-primary btn-round",
    attrs: {
      "type": "reset"
    },
    on: {
      "click": function($event) {
        _vm.cancel()
      }
    }
  }, [_vm._v("Cancel")]), _vm._v(" "), _c('button', {
    staticClass: "btn btn-primary btn-round",
    attrs: {
      "type": "button"
    },
    on: {
      "click": function($event) {
        _vm.add()
      }
    }
  }, [_vm._v("Submit")])])]), _vm._v(" "), _c('div', {
    staticClass: "clearfix"
  })])])])])])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "card-header",
    attrs: {
      "data-background-color": "purple"
    }
  }, [_c('h4', {
    staticClass: "title"
  }, [_vm._v(" Add service")]), _vm._v(" "), _c('p', {
    staticClass: "category"
  }, [_vm._v("Add service Information")])])
}]}

/***/ }),
/* 70 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "content"
  }, [_c('div', {
    staticClass: "container-fluid"
  }, [_c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-8"
  }, [_c('div', {
    staticClass: "card"
  }, [_vm._m(0), _vm._v(" "), _c('div', {
    staticClass: "card-content"
  }, [_c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-6"
  }, [_c('div', {
    staticClass: "form-group label-floating"
  }, [_c('label', {
    staticClass: "control-label"
  }, [_vm._v("Username")]), _vm._v(" "), _c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.username),
      expression: "username"
    }],
    staticClass: "form-control",
    attrs: {
      "type": "text",
      "name": "username",
      "id": "username",
      "required": ""
    },
    domProps: {
      "value": (_vm.username)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.username = $event.target.value
      }
    }
  })])]), _vm._v(" "), _c('div', {
    staticClass: "col-md-6"
  }, [_c('div', {
    staticClass: "form-group label-floating"
  }, [_c('label', {
    staticClass: "control-label"
  }, [_vm._v("Email address")]), _vm._v(" "), _c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.email),
      expression: "email"
    }],
    staticClass: "form-control",
    attrs: {
      "type": "email",
      "name": "email",
      "id": "email",
      "required": ""
    },
    domProps: {
      "value": (_vm.email)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.email = $event.target.value
      }
    }
  })])])]), _vm._v(" "), _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-6"
  }, [_c('div', {
    staticClass: "form-group label-floating"
  }, [_c('label', {
    staticClass: "control-label"
  }, [_vm._v("Fist Name")]), _vm._v(" "), _c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.firstName),
      expression: "firstName"
    }],
    staticClass: "form-control",
    attrs: {
      "type": "text",
      "name": "firstName",
      "id": "firstName",
      "required": ""
    },
    domProps: {
      "value": (_vm.firstName)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.firstName = $event.target.value
      }
    }
  })])]), _vm._v(" "), _c('div', {
    staticClass: "col-md-6"
  }, [_c('div', {
    staticClass: "form-group label-floating"
  }, [_c('label', {
    staticClass: "control-label"
  }, [_vm._v("Last Name")]), _vm._v(" "), _c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.lastName),
      expression: "lastName"
    }],
    staticClass: "form-control",
    attrs: {
      "type": "text",
      "name": "lastName",
      "id": "lastName",
      "required": ""
    },
    domProps: {
      "value": (_vm.lastName)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.lastName = $event.target.value
      }
    }
  })])])]), _vm._v(" "), _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-12"
  }, [_c('div', {
    staticClass: "form-group label-floating"
  }, [_c('label', {
    staticClass: "control-label"
  }, [_vm._v("Mobile")]), _vm._v(" "), _c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.mobile),
      expression: "mobile"
    }],
    staticClass: "form-control",
    attrs: {
      "type": "text",
      "name": "mobile",
      "id": "mobile",
      "required": ""
    },
    domProps: {
      "value": (_vm.mobile)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.mobile = $event.target.value
      }
    }
  })])])]), _vm._v(" "), _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-6"
  }, [_c('div', {
    staticClass: "form-group label-floating"
  }, [_c('label', {
    staticClass: "control-label"
  }, [_vm._v("Select Role")]), _vm._v(" "), _c('select', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.userrole),
      expression: "userrole"
    }],
    staticClass: "form-control",
    attrs: {
      "name": "userrole",
      "id": "userrole",
      "required": ""
    },
    on: {
      "change": function($event) {
        var $$selectedVal = Array.prototype.filter.call($event.target.options, function(o) {
          return o.selected
        }).map(function(o) {
          var val = "_value" in o ? o._value : o.value;
          return val
        });
        _vm.userrole = $event.target.multiple ? $$selectedVal : $$selectedVal[0]
      }
    }
  }, [_c('option', {
    attrs: {
      "value": "read"
    }
  }, [_vm._v("Read")]), _vm._v(" "), _c('option', {
    attrs: {
      "value": "write"
    }
  }, [_vm._v("Write")])])])]), _vm._v(" "), _c('div', {
    staticClass: "col-md-6"
  }, [_c('div', {
    staticClass: "form-group label-floating"
  }, [_c('label', {
    staticClass: "control-label"
  }, [_vm._v("Select Status")]), _vm._v(" "), _c('select', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.status),
      expression: "status"
    }],
    staticClass: "form-control",
    attrs: {
      "name": "status",
      "id": "status",
      "required": ""
    },
    on: {
      "change": function($event) {
        var $$selectedVal = Array.prototype.filter.call($event.target.options, function(o) {
          return o.selected
        }).map(function(o) {
          var val = "_value" in o ? o._value : o.value;
          return val
        });
        _vm.status = $event.target.multiple ? $$selectedVal : $$selectedVal[0]
      }
    }
  }, [_c('option', {
    attrs: {
      "value": "active"
    }
  }, [_vm._v("Active")]), _vm._v(" "), _c('option', {
    attrs: {
      "value": "inactive"
    }
  }, [_vm._v("InActive")])])])])]), _vm._v(" "), _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-6"
  }, [_c('div', {
    staticClass: "form-group label-floating"
  }, [_c('label', {
    staticClass: "control-label"
  }, [_vm._v("Password")]), _vm._v(" "), _c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.password),
      expression: "password"
    }],
    staticClass: "form-control",
    attrs: {
      "type": "password",
      "name": "password",
      "id": "password",
      "required": ""
    },
    domProps: {
      "value": (_vm.password)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.password = $event.target.value
      }
    }
  })])]), _vm._v(" "), _vm._m(1)]), _vm._v(" "), _c('button', {
    staticClass: "btn btn-primary pull-right",
    attrs: {
      "type": "submit"
    },
    on: {
      "click": function($event) {
        _vm.signup()
      }
    }
  }, [_c('b', [_vm._v(" SIGN UP ")])]), _vm._v(" "), _c('div', {
    staticClass: "clearfix"
  })])])])])])])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "card-header",
    attrs: {
      "data-background-color": "purple"
    }
  }, [_c('h4', {
    staticClass: "title"
  }, [_vm._v(" Add User")]), _vm._v(" "), _c('p', {
    staticClass: "category"
  }, [_vm._v("Add User Information")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "col-md-6"
  }, [_c('div', {
    staticClass: "form-group label-floating"
  }, [_c('label', {
    staticClass: "control-label"
  }, [_vm._v("Confirm Password")]), _vm._v(" "), _c('input', {
    staticClass: "form-control",
    attrs: {
      "type": "password",
      "name": "confirm",
      "id": "confirm",
      "required": ""
    }
  })])])
}]}

/***/ }),
/* 71 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "col-md-12",
    staticStyle: {
      "background-color": "white",
      "padding-top": "25px",
      "box-shadow": "10px 10px 5px black"
    }
  }, [_c('div', {
    staticClass: "content ",
    staticStyle: {
      "background-color": "white"
    }
  }, [_c('div', {
    staticClass: "pull-right"
  }, [_c('button', {
    staticClass: "btn btn-google btn-round btn-sm",
    attrs: {
      "type": "reset"
    },
    on: {
      "click": function($event) {
        _vm.cancelop()
      }
    }
  }, [_c('span', {
    staticClass: "glyphicon glyphicon-remove"
  })])]), _vm._v(" "), _c('div', {
    staticClass: "col-md-10"
  }, [_c('div', [_c('h5'), _vm._v(" "), _c('button', {
    staticClass: "btn btn-md btn-instagram"
  }, [_c('b', [_vm._v(_vm._s(_vm.APIs.apiType))])])]), _vm._v(" "), _c('div', {
    staticClass: "data"
  }, [_c('h6', [_c('b', [_vm._v("/" + _vm._s(_vm.APIs.url))])])]), _vm._v(" "), _c('div', [_vm._m(0), _vm._v(" "), _c('table', {
    staticClass: "table table-bordered"
  }, [_vm._m(1), _vm._v(" "), _c('tbody', _vm._l((_vm.APIs.headerParameter), function(header) {
    return _c('tr', [_c('td', [_vm._v(_vm._s(header.key))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(header.value))])])
  }))])]), _vm._v(" "), _c('div', [_vm._m(2), _vm._v(" "), _c('table', {
    staticClass: "table table-bordered"
  }, [_vm._m(3), _vm._v(" "), _c('tbody', _vm._l((_vm.APIs.params), function(para) {
    return _c('tr', {
      staticStyle: {
        "color": "black"
      }
    }, [_c('td', [_vm._v(_vm._s(para.field))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(para.type))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(para.description))])])
  }))])]), _vm._v(" "), _c('div', {
    staticClass: "data"
  }, [_vm._m(4), _vm._v(" "), _c('p', [_c('pre', [_vm._v(_vm._s(_vm.APIs.exampleUsage))])])]), _vm._v(" "), _c('div', {
    staticClass: "data"
  }, [_vm._m(5), _vm._v(" "), _c('p', [_c('pre', [_vm._v(_vm._s(_vm.APIs.successResponse))])])]), _vm._v(" "), _c('div', {
    staticClass: "data"
  }, [_vm._m(6), _vm._v(" "), _c('p', [_c('pre', [_vm._v(_vm._s(_vm.APIs.errorResponse))])])])])])])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('h4', [_c('b', [_vm._v("Header Paramter:")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('thead', {
    staticStyle: {
      "background-color": "grey",
      "color": "white"
    }
  }, [_c('tr', [_c('th', [_c('b', [_vm._v("Key")])]), _vm._v(" "), _c('th', [_c('b', [_vm._v("value ")])])])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('h4', [_c('b', [_vm._v(" Parameter:")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('thead', {
    staticStyle: {
      "background-color": "grey",
      "color": "white"
    }
  }, [_c('tr', [_c('th', [_c('b', [_vm._v("Field")])]), _vm._v(" "), _c('th', [_c('b', [_vm._v("Type")])]), _vm._v(" "), _c('th', [_c('b', [_vm._v("Description")])])])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('h4', [_c('b', [_vm._v("Example Usage:")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('h4', [_c('b', [_vm._v("Success Response:")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('h4', [_c('b', [_vm._v("Error Response:")])])
}]}

/***/ }),
/* 72 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('h1', [_vm._v("Myprojects")])
},staticRenderFns: []}

/***/ }),
/* 73 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "content"
  }, [_c('div', {
    staticClass: "container-fluid",
    attrs: {
      "id": "tb"
    }
  }, [_c('div', {
    staticClass: "row"
  }, [_c('input', {
    staticClass: "btn btn-md ",
    staticStyle: {
      "background-color": "#9c27b0",
      "color": "#FFFFFF"
    },
    attrs: {
      "type": "button",
      "value": "Add New User"
    },
    on: {
      "click": function($event) {
        _vm.addnewuser()
      }
    }
  }), _vm._v(" "), _c('div', {
    staticClass: "col-md-12"
  }, [_c('div', {
    staticClass: "card"
  }, [_vm._m(0), _vm._v(" "), _c('div', {
    staticClass: "card-content table-responsive"
  }, [_c('table', {
    staticClass: "table table-hover"
  }, [_vm._m(1), _vm._v(" "), _c('tbody', _vm._l((_vm.users), function(user) {
    return _c('tr', [_c('td', [_vm._v(_vm._s(user.firstName) + " " + _vm._s(user.lastName))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(user.userrole))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(user.status))]), _vm._v(" "), _c('input', {
      staticClass: "btn btn-sm  btn-danger",
      attrs: {
        "type": "button",
        "value": "Delete"
      },
      on: {
        "click": function($event) {
          _vm.deleteuser(user)
        }
      }
    }), _vm._v(" "), _c('input', {
      staticClass: "btn btn-sm  btn-primary",
      attrs: {
        "type": "button",
        "value": "Edit"
      },
      on: {
        "click": function($event) {
          _vm.div_show1(user)
        }
      }
    })])
  }))])])])])])]), _vm._v(" "), _c('div', {
    staticStyle: {
      "display": "none"
    },
    attrs: {
      "id": "frm1"
    }
  }, [_c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-8"
  }, [_c('div', {
    staticClass: "card"
  }, [_vm._m(2), _vm._v(" "), _c('div', {
    staticClass: "card-content"
  }, [_c('form', [_c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-12"
  }, [_c('div', {
    staticClass: "form-group label-floating"
  }, [_c('label', {
    staticClass: "control-label",
    attrs: {
      "for": "name"
    }
  }, [_vm._v("username")]), _vm._v(" "), _c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.username),
      expression: "username"
    }],
    staticClass: "form-control",
    attrs: {
      "type": "text",
      "name": "username",
      "required": ""
    },
    domProps: {
      "value": (_vm.username)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.username = $event.target.value
      }
    }
  })])])]), _vm._v(" "), _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-12"
  }, [_c('div', {
    staticClass: "form-group label-floating"
  }, [_c('label', {
    staticClass: "control-label",
    attrs: {
      "for": "name"
    }
  }, [_vm._v("firstName")]), _vm._v(" "), _c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.firstName),
      expression: "firstName"
    }],
    staticClass: "form-control",
    attrs: {
      "type": "text",
      "name": "firstName",
      "required": ""
    },
    domProps: {
      "value": (_vm.firstName)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.firstName = $event.target.value
      }
    }
  })])])]), _vm._v(" "), _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-12"
  }, [_c('div', {
    staticClass: "form-group label-floating"
  }, [_c('label', {
    staticClass: "control-label",
    attrs: {
      "for": "name"
    }
  }, [_vm._v("lastName")]), _vm._v(" "), _c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.lastName),
      expression: "lastName"
    }],
    staticClass: "form-control",
    attrs: {
      "type": "text",
      "name": "lastName",
      "required": ""
    },
    domProps: {
      "value": (_vm.lastName)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.lastName = $event.target.value
      }
    }
  })])])]), _vm._v(" "), _c('div', {
    staticClass: "col-md-12"
  }, [_c('div', {
    staticClass: "form-group label-floating"
  }, [_c('label', {
    staticClass: "control-label",
    attrs: {
      "for": "status"
    }
  }, [_vm._v("status")]), _vm._v(" "), _c('select', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.status),
      expression: "status"
    }],
    staticClass: "form-control",
    attrs: {
      "name": "status",
      "id": "status",
      "required": ""
    },
    on: {
      "change": function($event) {
        var $$selectedVal = Array.prototype.filter.call($event.target.options, function(o) {
          return o.selected
        }).map(function(o) {
          var val = "_value" in o ? o._value : o.value;
          return val
        });
        _vm.status = $event.target.multiple ? $$selectedVal : $$selectedVal[0]
      }
    }
  }, [_c('option', {
    attrs: {
      "value": "active"
    }
  }, [_vm._v("Active")]), _vm._v(" "), _c('option', {
    attrs: {
      "value": "inactive"
    }
  }, [_vm._v("InActive")])])])]), _vm._v(" "), _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-12"
  }, [_c('div', {
    staticClass: "form-group label-floating"
  }, [_c('label', {
    staticClass: "control-label",
    attrs: {
      "for": "purpose"
    }
  }, [_vm._v("email")]), _vm._v(" "), _c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.email),
      expression: "email"
    }],
    staticClass: "form-control",
    attrs: {
      "type": "email",
      "name": "email",
      "required": ""
    },
    domProps: {
      "value": (_vm.email)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.email = $event.target.value
      }
    }
  })])])]), _vm._v(" "), _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-12"
  }, [_c('div', {
    staticClass: "form-group label-floating"
  }, [_c('label', {
    staticClass: "control-label",
    attrs: {
      "for": "authenticationDescription"
    }
  }, [_vm._v("mobile")]), _vm._v(" "), _c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.mobile),
      expression: "mobile"
    }],
    staticClass: "form-control",
    attrs: {
      "type": "number",
      "name": "mobile",
      "required": ""
    },
    domProps: {
      "value": (_vm.mobile)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.mobile = $event.target.value
      }
    }
  })])])]), _vm._v(" "), _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-12"
  }, [_c('div', {
    staticClass: "form-group label-floating"
  }, [_c('label', {
    staticClass: "control-label",
    attrs: {
      "for": "usertype"
    }
  }, [_vm._v("usertype")]), _vm._v(" "), _c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.usertype),
      expression: "usertype"
    }],
    staticClass: "form-control",
    attrs: {
      "type": "text",
      "name": "usertype",
      "required": ""
    },
    domProps: {
      "value": (_vm.usertype)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.usertype = $event.target.value
      }
    }
  })])])]), _vm._v(" "), _c('div', {
    staticClass: "col-md-12"
  }, [_c('div', {
    staticClass: "form-group label-floating"
  }, [_c('label', {
    staticClass: "control-label",
    attrs: {
      "for": "userrole"
    }
  }, [_vm._v("userrole")]), _vm._v(" "), _c('select', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.userrole),
      expression: "userrole"
    }],
    staticClass: "form-control",
    attrs: {
      "name": "userrole",
      "id": "userrole",
      "required": ""
    },
    on: {
      "change": function($event) {
        var $$selectedVal = Array.prototype.filter.call($event.target.options, function(o) {
          return o.selected
        }).map(function(o) {
          var val = "_value" in o ? o._value : o.value;
          return val
        });
        _vm.userrole = $event.target.multiple ? $$selectedVal : $$selectedVal[0]
      }
    }
  }, [_c('option', {
    attrs: {
      "value": "read"
    }
  }, [_vm._v("Read")]), _vm._v(" "), _c('option', {
    attrs: {
      "value": "write"
    }
  }, [_vm._v("Write")])])])]), _vm._v(" "), _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticStyle: {
      "margin-left": "30%"
    }
  }, [_c('button', {
    staticClass: "btn btn-primary btn-round",
    attrs: {
      "type": "reset"
    },
    on: {
      "click": function($event) {
        _vm.cancelop()
      }
    }
  }, [_vm._v("Cancel")]), _vm._v(" "), _c('button', {
    staticClass: "btn btn-primary btn-round",
    attrs: {
      "type": "button"
    },
    on: {
      "click": function($event) {
        _vm.div_hide1()
      }
    }
  }, [_vm._v("Submit")])])]), _vm._v(" "), _c('div', {
    staticClass: "clearfix"
  })])])])])])])])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "card-header",
    attrs: {
      "data-background-color": "purple"
    }
  }, [_c('h4', {
    staticClass: "title"
  }, [_vm._v("User LIST")]), _vm._v(" "), _c('p', {
    staticClass: "category"
  }, [_vm._v("Following is the User list      ")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('thead', [_c('th', [_vm._v(" Name")]), _vm._v(" "), _c('th', [_vm._v("Role")]), _vm._v(" "), _c('th', [_vm._v("Status")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "card-header",
    attrs: {
      "data-background-color": "purple"
    }
  }, [_c('h4', {
    staticClass: "title"
  }, [_vm._v(" Edit User")]), _vm._v(" "), _c('p', {
    staticClass: "category"
  }, [_vm._v("Edit User Information")])])
}]}

/***/ }),
/* 74 */,
/* 75 */
/***/ (function(module, exports) {

/* (ignored) */

/***/ })
],[27]);
//# sourceMappingURL=app.e24e56ee59f310ce157a.js.map
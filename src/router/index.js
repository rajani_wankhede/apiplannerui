import Vue from 'vue'
import Router from 'vue-router'
import Hello from '@/components/Hello'
import login from '@/components/login'
import signup from '@/components/signup'
import addnewuser from '@/components/addnewuser'
import adduser from '@/components/adduser'
import dashboard from '@/components/dashboard'
import allprojects from '@/components/allprojects'
import myprojects from '@/components/myprojects'
import addproject from '@/components/addproject'
import services from '@/components/services'
import addservice from '@/components/addservice'
import api from '@/components/api'
import addAPI from '@/components/addAPI'
import documentation from '@/components/documentation'
import index from '@/components/index'

Vue.use(Router)
export default new Router({
    routes: [{
            path: '/',
            name: 'login',
            component: login
        },
        {
            path: '/signup',
            name: 'signup',
            component: signup
        },
        {
            path: '/dashboard',
            name: 'dashboard',
            component: dashboard,
            children: [
                { path: 'index', name: 'index', component: index },
                { path: 'allprojects', name: 'allprojects', component: allprojects },
                { path: 'myprojects', name: 'myprojects', component: myprojects },
                { path: 'addproject', name: 'addproject', component: addproject },
                { path: 'addnewuser', name: 'addnewuser', component: addnewuser },
                { path: 'adduser', name: 'adduser', component: adduser },
                //  { path: 'services', name: 'services', component: services },
                { path: 'services/:id', name: 'services', component: services },
                { path: 'addservice/:id', name: 'services', component: addservice },
                { path: 'api/:id', name: 'api', component: api },
                { path: 'documentation/:id', name: 'documentation', component: documentation },
                { path: 'addAPI/:id', name: 'addAPI', component: addAPI },
            ]
        },
    ]
})